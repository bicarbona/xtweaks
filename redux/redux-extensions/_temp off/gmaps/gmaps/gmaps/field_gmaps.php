<?php
/**
 * Redux Framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * any later version.
 *
 * Redux Framework is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Redux Framework. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package     ReduxFramework
 * @author      Dovy Paukstys
 * @version     3.1.5
 */

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) exit;

// Don't duplicate me!
if( !class_exists( 'ReduxFramework_gmaps' ) ) {

    /**
     * Main ReduxFramework_gmaps class
     *
     * @since       1.0.0
     */
    class ReduxFramework_gmaps extends ReduxFramework {
    
        /**
         * Field Constructor.
         *
         * Required - must call the parent constructor, then assign field and value to vars, and obviously call the render field function
         *
         * @since       1.0.0
         * @access      public
         * @return      void
         */
        function __construct( $field = array(), $value ='', $parent ) {
        
            
            $this->parent = $parent;
            $this->field = $field;
            $this->value = $value;

            if ( empty( $this->extension_dir ) ) {
                $this->extension_dir = trailingslashit( str_replace( '\\', '/', dirname( __FILE__ ) ) );
                $this->extension_url = site_url( str_replace( trailingslashit( str_replace( '\\', '/', ABSPATH ) ), '', $this->extension_dir ) );
            }    

            // Set default args for this field to avoid bad indexes. Change this to anything you use.
            $defaults = array(
                'options'           => array(),
                'stylesheet'        => '',
                'output'            => true,
                'enqueue'           => true,
                'enqueue_frontend'  => true
            );

            //Initialise
            $this->field = wp_parse_args( $this->field, $defaults );
        
        }

        /**
         * Field Render Function.
         *
         * Takes the vars and outputs the HTML for the field in the settings
         *
         * @since       1.0.0
         * @access      public
         * @return      void
         */
        public function render() {
        
            // HTML output goes here

            //Save args
            if ( ! empty( $this->field['data'] ) && empty( $this->field['options'] ) ) {
                if ( empty( $this->field['args'] ) ) {
                    $this->field['args'] = array();
                }

                $this->field['options'] = $this->parent->get_wordpress_data( $this->field['data'], $this->field['args'] );
                $this->field['class'] .= " hasOptions ";
            }

            //Save data
            if ( empty( $this->value ) && ! empty( $this->field['data'] ) && ! empty( $this->field['options'] ) ) {
                $this->value = $this->field['options'];
            }

            $placeholder = '';

            $qtip_title = isset( $this->field['text_hint']['title'] ) ? 'qtip-title="' . $this->field['text_hint']['title'] . '" ' : '';
            $qtip_text  = isset( $this->field['text_hint']['content'] ) ? 'qtip-content="' . $this->field['text_hint']['content'] . '" ' : '';
            $readonly       = ( isset( $this->field['readonly'] ) && $this->field['readonly']) ? ' readonly="readonly"' : '';
            $autocomplete   = ( isset($this->field['autocomplete']) && $this->field['autocomplete'] == false) ? ' autocomplete="off"' : ''; 

            $lat = isset($this->value['lat'])? $this->value['lat']: '';
            $lng = isset($this->value['lng'])? $this->value['lng']: '';

            $usestreetview = isset($this->value['street_visible'])? $this->value['street_visible']: 1;
            $street_lat = isset($this->value['street_lat'])? $this->value['street_lat']: 0;
            $street_lng = isset($this->value['street_lng'])? $this->value['street_lng']: 0;
            $street_heading = isset($this->value['street_heading'])? $this->value['street_heading']: 0;
            $street_pitch = isset($this->value['street_pitch'])? $this->value['street_pitch']: 0;
            $street_zoom = isset($this->value['street_zoom'])? $this->value['street_zoom']: 0;

            //Put values as json in DOM so javascript can use them
            $jsonvalues = array(
                'field' => $this->field,
                'value' => $this->value
            );
            if ( version_compare( phpversion(), "5.3.0", ">=" ) ) {
                $json = json_encode( $jsonvalues, true );
            } else {
                $json = json_encode( $jsonvalues );
            }

        ob_start();

?>
            <table style="width:100%;">
                <tbody>
                    <tr>
                        <td colspan="2">
                            <div class="map-wrapper">
                                <input class="loca_txt_find_address" type="text"><a class="button loca_btn_find_address"><?php _e('Find', THEMENAME);?></a>
                                <div class="loca-item-map-container"></div>
                                <div id="loca-gmaps-object-json" class="hide"><?php echo $json; ?></div>
                            </div>                       
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?php
                            echo '<div class="input_wrapper">';
                            echo '<label for="' . $this->field['id'] . '-gmaps-lat">Latitude</label> ';
                            echo '<input ' . $qtip_title . $qtip_text . 'type="text" id="' . $this->field['id'] . '-gmaps-lat" name="' . $this->field['name'] . $this->field['name_suffix'] . '[lat]' . '" ' . $placeholder . 'value="' . esc_attr( $lat ) . '" class="col-text ' . $this->field['class'] . '"' . $readonly . $autocomplete . ' /><br />';
                            echo '</div>';
                            ?>
                        </td>
                        <td>
                            <?php
                            echo '<div class="input_wrapper">';
                            echo '<label for="' . $this->field['id'] . '-gmaps-lng">Longitude</label> ';
                            echo '<input ' . $qtip_title . $qtip_text . 'type="text" id="' . $this->field['id'] . '-gmaps-lng" name="' . $this->field['name'] . $this->field['name_suffix'] . '[lng]' . '" ' . $placeholder . 'value="' . esc_attr( $lng ) . '" class="col-text ' . $this->field['class'] . '"' . $readonly . $autocomplete . ' /><br />';
                            echo '</div>';
                            ?>
                        </td>
                    </tr>
                </tbody>
            </table>


            <table style="width:100%;">
                <tbody>
                    <tr>
                        <td colspan="2">
                            <div class="streetview-wrapper">
                                <div class="redux_field_th">
                                    <?php _e("StreetView", THEMENAME);?>
                                </div>

                                <label>
                                    <input type="checkbox" name="<?php echo $this->field['name'] . $this->field['name_suffix']; ?>[street_visible]" value="1" <?php checked(1 == $usestreetview );?>>
                                    <?php _e("Use StreetView", THEMENAME);?>
                                </label>

                                <div class="loca-item-streetview-container<?php echo $usestreetview == 0? ' hidden': '';?>"></div>
                                <fieldset class="hidden">
                                    <?php
                                    echo "Latitude : <input name='".$this->field['name'] . $this->field['name_suffix']."[street_lat]' value='$street_lat' type='text'>";
                                    echo "Longitude : <input name='".$this->field['name'] . $this->field['name_suffix']."[street_lng]' value='$street_lng' type='text'>";
                                    echo "Heading : <input name='".$this->field['name'] . $this->field['name_suffix']."[street_heading]' value='$street_heading' type='text'>";
                                    echo "pitch: <input name='".$this->field['name'] . $this->field['name_suffix']."[street_pitch]' value='$street_pitch' type='text'>";
                                    echo "zoom : <input name='".$this->field['name'] . $this->field['name_suffix']."[street_zoom]' value='$street_zoom' type='text'>"; ?>
                                </fieldset>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>

<?php
        ob_end_flush();

        }
    
        /**
         * Enqueue Function.
         *
         * If this field requires any scripts, or css define this function and register/enqueue the scripts/css
         *
         * @since       1.0.0
         * @access      public
         * @return      void
         */
        public function enqueue() {

            //$extension = ReduxFramework_extension_gmaps::getInstance();
        
            wp_enqueue_script(
                'redux-field-gmaps-js', 
                $this->extension_url . 'field_gmaps.js', 
                array( 'jquery' ),
                '0.0.1',//time(),
                true
            );
        
            // wp_enqueue_script(
            //     'google-map'
            //     , "http://maps.googleapis.com/maps/api/js?sensor=false&libraries=places&{$loca_google_api}"
            //     , array ('jquery')
            //     , "0.0.1"
            //     , false
            // );
        
            wp_enqueue_script(
                'google_map_API'
                , "//maps.google.com/maps/api/js?sensor=true&amp;v=3&amp;language=en"
                , null
                , "0.0.1"
                , false
            );
        
            wp_enqueue_script(
                'gmap-v6'
                , $this->extension_url . "gmap3.min.js"
                , array ( 'jquery')
                , '6.0'
                , false
            );

            wp_enqueue_style(
                'redux-field-gmaps-css', 
                $this->extension_url . 'field_gmaps.css',
                time(),
                true
            );
        
        }
        
        /**
         * Output Function.
         *
         * Used to enqueue to the front-end
         *
         * @since       1.0.0
         * @access      public
         * @return      void
         */        
        public function output() {

            if ( $this->field['enqueue_frontend'] ) {
                echo 'Gmaps frontend output';
            }
            
        }        
        
    }
}
