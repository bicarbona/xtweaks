## Global Fixes

~~~css
/** Global Fixes **/

/** Nested List  - pridat nastavenie cez redux **/
li > ol li{
    list-style-type: decimal;
}

/** Blockqoute p **/
blockquote p {
    margin-bottom: 0px !important;
}

/** Child menu zarovnane na pravo **/ 
ul.menu li:last-child > a{
    border-right: none !important;
}
~~~