<?php
/**
*   Plugin Name: Xtweaks
*   Plugin URI:  https://bitbucket.org/bicarbona/xtweaks
*   Description: Xtweaks - admin css, plugins etc...
*   Version:     0.6.9
*   Author:      Bicarbona
*   Author URI:
*   License:     GPLv2+
*   Text Domain: xtweaks
*   Domain Path: /languages
*   Bitbucket Plugin URI: https://bitbucket.org/bicarbona/xtweaks
*   Bitbucket Branch: master
**/

$custom_theme_plugin_text_domain = 'xtheme-nehnutelnosti';

// urls
// 'basename'			=> plugin_basename( __FILE__ ),
// 'path'				=> plugin_dir_path( __FILE__ ),
// 'dir'				=> plugin_dir_url( __FILE__ ),

// echo plugin_basename( __FILE__ );
// echo plugin_dir_url( __FILE__ );

define( 'XTW_PLUGIN_PATH',  plugin_dir_path( __FILE__ ));
define( 'XTW_URL_PATH',  plugin_dir_url( __FILE__ ));

define( 'XTW_SLUG', trim( dirname( plugin_basename( __FILE__ ) ), '/' ) );
define( 'XTW_URL', plugins_url() . '/' . XTW_SLUG );

// if ( ! defined( 'XTW_SLUG' ) ) {
// }

include 'lib/helper-functions.php';

// is_object( $value ) ) {
//   var_dump( $value );


/**
*
*   Add link to settings
*   in plugin listing
*
**/

add_filter( 'plugin_action_links_' . plugin_basename(__FILE__), 'xtweaks_action_links' );

function xtweaks_action_links( $links ) {
   $links[] = '<a href="'. esc_url( get_admin_url(null, 'admin.php?page=Tweaks') ) .'">Settings</a>';
   // $links[] = '<a href="'. esc_url( get_admin_url(null, 'update-core.php') ) .'">Updates</a>';
  $links[] = '<a href="'. esc_url( get_admin_url(null, 'update-core.php?force-check=1') ) .'">Updates Check</a>';

   // http://p4u/wp-admin/update-core.php?force-check=1
   return $links;
}


/**
*
*   Vypnutie / zapnutie LESS
*   url => ?lessoff
*
**/

if(isset($_GET['lessoff'])) {

    define('INC_LESS', false);

} else {

    define('INC_LESS', true);

}


/**
*
*   Debug Mode [actualy not work]
*
**/

if(isset($_GET['debug'])){
    include_once 'lib/debug.php';
}


/**
*
*   Redux config
*
**/

if ( file_exists( dirname( __FILE__ ) . '/sections/redux-config-main-tweaks.php' ) ) {
    // require_once( dirname( __FILE__ ) . '/sections/sample-config.php' );
    require_once( dirname( __FILE__ ) . '/sections/redux-config-main-tweaks.php' );

}

/**
*
*   Call options
*
**/

$options = get_option('redux_tweaks');
global $options;

//print_r ($options);


/**
*
*   Load Custom Redux Admin panels
*   Header
*
**/

function filter_redux_panel_header(){
    $dir = plugin_dir_path( __FILE__ );
        $path  = $dir .'redux/template/panel/header.tpl.php';
    return $path;
}

add_filter( 'redux/redux_tweaks/panel/template/header.tpl.php', 'filter_redux_panel_header' );

global $options;

if( $options['opt-shiftnav-plugin'] ){
  include 'plugins/responsive-mobile-menu/shiftnav-responsive-mobile-menu.php';
}

include 'plugins/duplicate-menu/duplicate-menu.php';

// Post type archive links in Menu
include 'plugins/post-type-archive-links/post-type-archive-links.php';


// Widget Alias
// include 'plugins/widget-alias/widget-alias.php';


/**
*
*   Load Tweaks Modules
*
**/


/* Directories that contain Tweaks */
$classesDir = array (
    '/tweaks/plugins/',
     '/tweaks/frontend/',
     '/tweaks/less/',
     '/tweaks/admin/',
     '/tweaks/both/',
    // '/tweaks/test/',
);

function autoload_tweaks() {

    global $classesDir;

    foreach ($classesDir as $directory) {
      //  print_r ($directory);

      foreach ( glob( __DIR__ . $directory.'*.php' ) as $my_theme_filename ) {

        // print_r ($my_theme_filename);
          if (!strpos($my_theme_filename, '-sample') ) {

            include_once ( $my_theme_filename );
          }
      }

    }

// $inclpath = 'tweaks/admin/';
// include $inclpath.'admin-widgets-manage.php';


}

autoload_tweaks( );

/**
*
*   Options to LESS
*
**/

if( INC_LESS == TRUE ){ // Turn OFF -> url => ?lessoff

    require_once( dirname( __FILE__ ) . '/lib/option-to-less.php' );

}


/**
*   Etienne
*   view-builder-class
*
**/

include('lib/view-builder-class.php');


function xtw_return_taxonomies() {
  $args       = array( 'public' => true, '_builtin' => false );
  $output     = 'names';
  $taxonomies = get_taxonomies( $args, $output );

  return $taxonomies;
}


function xtw_array_to_options_list( $source_arr, $selected ) {
  foreach ( $source_arr as $key => $value ) {
    echo '<option value="' . esc_attr( $key ) . '" ' . ( $selected == $key ? 'selected' : null ) . '>' . esc_attr( $value ) . '</option>';
  }
}

/**
* @param $post_id
* @param $meta_string
*
* @return array
**/

function xtw_get_post_terms( $post_id, $meta_string ) {

  $post_tax_terms = array();
  $meta_custom    = substr_count( $meta_string, '%ct:' );
  preg_match_all( "/(?<=\\%)(ct\\:)(.*)(?=\\%)/uiUmx", $meta_string, $matches );
  for ( $i = 1; $i <= $meta_custom; $i ++ ) {
    if ( taxonomy_exists( $matches[ 2 ][ $i - 1 ] ) ) {
      $post_tax_terms[] = array( $matches[ 2 ][ $i - 1 ] => get_the_term_list( $post_id, $matches[ 2 ][ $i - 1 ], null, ', ', null ) );
    }
  }

  return $post_tax_terms;
}


// Unused until can find a way to make this load before Blueprints editor
function xtw_set_tax_titles( $pzarc_tax = '' ) {
  global $pzarc_taxonomy_list;
  $pzarc_taxonomy_list = array();
  $pzarc_taxes         = get_taxonomies( array( 'public' => true ) );
  foreach ( $pzarc_taxes as $k => $v ) {
    $pzarc_tax                 = get_taxonomy( $k );
    $pzarc_taxonomy_list[ $k ] = $pzarc_tax->labels->name;
  }
}


function xtw_term_title( $appendage, $terms ) {
  $term_list = '';
  foreach ( $terms->queries as $term_query ) {
    foreach ( $term_query[ 'terms' ] as $term_tag ) {
      $term_object = get_terms( $term_query[ 'taxonomy' ], array( 'slug' => $term_tag ) );
      $term_list .= ', ' . $term_object[ 0 ]->name;
    }
  }

  return $appendage . substr( $term_list, 2 );
}


function xtw_get_terms( $taxonomy = '', $args = array(), $array = true ) {

  $terms = get_terms( $taxonomy, $args );
  if ( isset( $terms->errors ) ) {
    return null;
  } else {
    if ( $array && ! empty( $terms ) ) {
      $term_list = array();
      foreach ( $terms as $k => $v ) {
        $term_list[ $v->slug ] = $v->name;
      }
      $terms = $term_list;
    }

    return $terms;
  }
}


function xtw_get_taxonomies( $catstags = true, $has_blank = true ) {
  $taxonomy_list = get_taxonomies( array(
                                     'public'   => true,
                                     '_builtin' => false
                                   ) );
  foreach ( $taxonomy_list as $k => $v ) {
    $tax_obj             = get_taxonomy( $k );
    $taxonomy_list[ $k ] = $tax_obj->labels->name;
  }
  // Add the None option if required
  $extras        = $has_blank ? array(
    0          => '',
    'category' => 'Categories',
    'post_tag' => 'Tags'
  )
    : array( 'category' => 'Categories', 'post_tag' => 'Tags' );
  $taxonomy_list = $catstags ? $extras + $taxonomy_list : $taxonomy_list;

  return $taxonomy_list;
}


/**
*
*
**/

function xtw_get_tags() {
  $return_arr = array();
  if ( ! empty( $_GET[ 'post' ] ) ) {
    $ctname       = '';
    $thispostmeta = get_post_meta( $_GET[ 'post' ] );
    $ctname       = ( ! empty( $thispostmeta[ '_content_general_other-tax' ][ 0 ] ) ? $thispostmeta[ '_content_general_other-tax' ][ 0 ] : null );
    if ( $ctname ) {
      $args       = array( 'hide_empty' => false );
      $custom_tax = get_terms( $ctname, $args );
      foreach ( $custom_tax as $ct ) {
        $return_arr[ $ct->slug ] = $ct->name;
      }
    }
  }

  return $return_arr;
}


// print_r($thispostmeta);
// var_dump($_GET[ 'post' ]);

function xtw_get_tags_2() {
  $return_arr = array();
  // if ( ! empty( $_GET[ 'post' ] ) ) {
    $ctname       = '';

    /**
    * $_GET[ 'post' ] aktualne ID tohto Query - >
    */
    $thispostmeta = get_post_meta( $_GET[ 'post' ] );

    $tax_name  = ( ! empty( $thispostmeta[ 'vb-other-tax' ][ 0 ] ) ? $thispostmeta[ 'vb-other-tax' ][ 0 ] : null );

    // $tax_name  = 'property-categories';

    if ( $tax_name ) {
      $args       = array( 'hide_empty' => false );
      $custom_tax = get_terms( $tax_name, $args );
      foreach ( $custom_tax as $ct ) {
        $return_arr[ $ct->slug ] = $ct->name;
      }
    }
  // }

  return $return_arr;
}


?>
