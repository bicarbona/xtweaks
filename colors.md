http://learnsemantic.com/developing/customizing.html  
http://semantic-ui.com/elements/button.html
https://webdesign.tutsplus.com/articles/quick-tip-name-your-sass-variables-modularly--webdesign-13364

+ color-primary
+ color-secondary
+ color-brand
+ color-action
+ color-neutral
+ color-accent

fill
text

## Colors
+ red            : #B03060;
+ orange         : #FE9A76;
+ yellow         : #FFD700;
+ olive          : #32CD32;
+ green          : #016936;
+ teal           : #008080;
+ blue           : #0E6EB8;
+ violet         : #EE82EE;
+ purple         : #B413EC;
+ pink           : #FF1493;
+ brown          : #A52A2A;
+ grey           : #A0A0A0;
+ black          : #000000;
+ cyan

	
## Notice / alert
+ color-error
+ color-warning
+ color-danger
+ color-success
+ color-info

## Status - funkciou (dark, lighten etc)

+ color-inactive
+ color-active
+ color-focus

## UI
+ color-shadow
+ color-border
+ boxShadow

$border-default: 1px solid black;


## Text
+ color-text
+ color-text-strong
+ color-text-heading
+ color-text-invert
+ color-text-strong-invert
+ color-text-weak

+sidebar-text-color

##
+ textShadow
+ textTransform
muted

## Buttons
color-button-focus
color-button-active
color-button-hover
color-button-inactive

## Background
color-background
color-background-shade
color-background-invert

## Link
color-text-text-link
color-text-link-focus
color-text-link-active
color-text-link-visited
color-text-link-hover

##xx

brand-green: hsl(60, 30%, 50%);
light-border: lighten(brand-green, 30%);

bright-text: hsl(0, 0%, 50%);
dark-text: hsl(0, 0%, 90%);