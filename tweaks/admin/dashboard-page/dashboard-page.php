<?php

/**
*
* Custom Dashboard Page
*
*/

/** WordPress Administration Bootstrap */
require_once( ABSPATH . 'wp-load.php' );
require_once( ABSPATH . 'wp-admin/admin.php' );
require_once( ABSPATH . 'wp-admin/admin-header.php' );
?>

<div class="wrap about-wrap">

<h2><?php _e( 'Vitaj' ); ?></h2>

<script>
	jQuery( function($) {
	    $( "#dashboard-tabs" ).tabs();
	} );
</script>
<div id="dashboard-tabs">
	<ul>
		<li><a class="nav-tab" href="#tabs-1">Tab 1</a></li>
		<li><a class="nav-tab" href="#tabs-2">Tab 2</a></li>
		<li><a class="nav-tab" href="#tabs-3">Tab 3</a></li>
	</ul>
<div class="tabs content">
	<div id="tabs-1">


		<?php if (class_exists('wp_less')) {
			# code...
			echo 'LESS OK';

		} else {
				echo 'Check for LESS Plugin <a href="/wp-admin/plugins.php">PLUGINS</a>';

		}

		?>
<pre>

define( 'BACKUPBUDDY_API_ENABLE', true ); // Enable BackupBuddy Deployment access.

// enable the Airplane Mode toggle
define('WPLT_AIRPLANE', 'true');
define('WPLT_SERVER', 'development');
define('WPLT_COLOR', '#007bec');

// define('WP_ENV', 'development');

$envs = [
	 'development'  => 'http://local-1',
	 'stage'        => 'http://local-2',
	 'production'   => 'http://example.com'
];

define('ENVIRONMENTS', serialize($envs));

//View Builder Panic Mode
define('VIEW_BUILDER_PANIC_MODE', true);

// Debug
if( 'root' === DB_USER ){

@ini_set( 'docref_root', 'http://php.net/manual/' );
@ini_set( 'docref_ext', '.php' );
@ini_set( 'error_log', dirname(__FILE__) . '/wp-content/php-errors.log' );

@ini_set( 'error_prepend_string', '<span style="color: #ff0000; background-color: transparent;">' );
@ini_set( 'error_append_string', '</span>' );

@ini_set( 'log_errors', 1 );
@ini_set( 'display_errors',0 );

@ini_set( 'error_reporting', E_ALL ^ E_NOTICE );

    define('WP_DEBUG',         true);  // Turn debugging ON
    define('WP_DEBUG_DISPLAY', false); // Turn forced display OFF
    define('WP_DEBUG_LOG',     true);  // Turn logging to wp-content/debug.log ON

}


</pre>


	</div>
	<div id="tabs-2">
		<p>Morbi tincidunt, dui sit amet facilisis feugiat, odio metus gravida ante, ut pharetra massa metus id nunc. Duis scelerisque molestie turpis. Sed fringilla, massa eget luctus malesuada, metus eros molestie lectus, ut tempus eros massa ut dolor. Aenean aliquet fringilla sem. Suspendisse sed ligula in ligula suscipit aliquam. Praesent in eros vestibulum mi adipiscing adipiscing. Morbi facilisis. Curabitur ornare consequat nunc. Aenean vel metus. Ut posuere viverra nulla. Aliquam erat volutpat. Pellentesque convallis. Maecenas feugiat, tellus pellentesque pretium posuere, felis lorem euismod felis, eu ornare leo nisi vel felis. Mauris consectetur tortor et purus.</p>
	</div>
	<div id="tabs-3">
		<p>Mauris eleifend est et turpis. Duis id erat. Suspendisse potenti. Aliquam vulputate, pede vel vehicula accumsan, mi neque rutrum erat, eu congue orci lorem eget lorem. Vestibulum non ante. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Fusce sodales. Quisque eu urna vel enim commodo pellentesque. Praesent eu risus hendrerit ligula tempus pretium. Curabitur lorem enim, pretium nec, feugiat nec, luctus a, lacus.</p>
		<p>Duis cursus. Maecenas ligula eros, blandit nec, pharetra at, semper at, magna. Nullam ac lacus. Nulla facilisi. Praesent viverra justo vitae neque. Praesent blandit adipiscing velit. Suspendisse potenti. Donec mattis, pede vel pharetra blandit, magna ligula faucibus eros, id euismod lacus dolor eget odio. Nam scelerisque. Donec non libero sed nulla mattis commodo. Ut sagittis. Donec nisi lectus, feugiat porttitor, tempor ac, tempor vitae, pede. Aenean vehicula velit eu tellus interdum rutrum. Maecenas commodo. Pellentesque nec elit. Fusce in lacus. Vivamus a libero vitae lectus hendrerit hendrerit.</p>
	</div>
</div>
</div>
</div>

<?php include( ABSPATH . 'wp-admin/admin-footer.php' );
