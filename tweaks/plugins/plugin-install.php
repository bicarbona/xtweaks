<?php
    /**
     * This file represents an example of the code that themes would use to register
     * the required plugins.
     * It is expected that theme authors would copy and paste this code into their
     * functions.php file, and amend to suit.
     *
     * @package       TGM-Plugin-Activation
     * @subpackage    Example
     * @version       2.3.6
     * @author        Thomas Griffin <thomas@thomasgriffinmedia.com>
     * @author        Gary Jones <gamajo@gamajo.com>
     * @copyright     Copyright (c) 2012, Thomas Griffin
     * @license       http://opensource.org/licenses/gpl-2.0.php GPL v2 or later
     * @link          https://github.com/thomasgriffin/TGM-Plugin-Activation
     */

    /**
     * Include the TGM_Plugin_Activation class.
     */
    // require_once dirname( __FILE__ ) . '/class-tgm-plugin-activation.php';

    add_action( 'tgmpa_register', 'xtw_register_required_plugins' );
    /**
     * Register the required plugins for this theme.
     * In this example, we register two plugins - one included with the TGMPA library
     * and one from the .org repo.
     * The variable passed to tgmpa_register_plugins() should be an array of plugin
     * arrays.
     * This function is hooked into tgmpa_init, which is fired within the
     * TGM_Plugin_Activation class constructor.
     */
    function xtw_register_required_plugins() {

        /**
         * Array of plugin arrays. Required keys are name and slug.
         * If the source is NOT from the .org repo, then source is also required.
         */
        $plugins = array(
             array(
                'name'               => 'Flex slider',
                // The plugin name
                'slug'               => 'headway-flex-slider',
                // The plugin slug (typically the folder name)
                'source'             => 'https://bitbucket.org/bicarbona/headway-flex-slider/get/master.zip',
                // The plugin source
                'required'           => false,
                // If false, the plugin is only 'recommended' instead of required
                'version'            => '',
                // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
                'force_activation'   => false,
                // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
                'force_deactivation' => false,
                // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
                // 'external_url'       => 'https://bitbucket.org/bicarbona/headway-flex-slider/get/a620e8e6cdb1.zip',
                // If set, overrides default API URL and points to an external URL
            ),

             array(
                'name'               => '@github-updater',
                // The plugin name
                'slug'               => 'github-updater',
                // The plugin slug (typically the folder name)
                'source'             => 'https://github.com/afragen/github-updater/archive/develop.zip',
                // The plugin source
                'required'           => false,
                // If false, the plugin is only 'recommended' instead of required
                'version'            => '',
                // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
                'force_activation'   => false,
                // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
                'force_deactivation' => false,
                // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
                // 'external_url'       => 'https://bitbucket.org/bicarbona/headway-flex-slider/get/a620e8e6cdb1.zip',
                // If set, overrides default API URL and points to an external URL
            ),

            // array(
            //     'name'               => 'WooCommerce Quickview',
            //     // The plugin name
            //     'slug'               => 'jck_woo_quickview',
            //     // The plugin slug (typically the folder name)
            //     'source'             => 'http://cardinal.swiftideas.com/extras/plugins/jck_woo_quickview.zip',
            //     // The plugin source
            //     'required'           => false,
            //     // If false, the plugin is only 'recommended' instead of required
            //     'version'            => '',
            //     // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
            //     'force_activation'   => false,
            //     // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
            //     'force_deactivation' => false,
            //     // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
            //     'external_url'       => '',
            //     // If set, overrides default API URL and points to an external URL
            // ),
             array(
                'name'     => 'Contact Form 7',
                'slug'     => 'contact-form-7',
                'required' => false,
                'force_activation'   => false,
            ),

             array(
                'name'     => 'Contact Form Honeypot',
                'slug'     => 'contact-form-7-honeypot',
                'required' => false,
                'force_activation'   => false,
            ),

             array(
                'name'     => 'Mpdf',
                'slug'     => 'wp-mpdf',
                'required' => false,
                'force_activation'   => false,
            ),

             array(
                'name'     => 'Syntax Highlighter',
                'slug'     => 'html-editor-syntax-highlighter',
                'required' => false,
                'force_activation'   => false,
            ),

             array(
                'name'     => 'Search Regex',
                'slug'     => 'search-regex',
                'required' => false,
                'force_activation'   => false,
            ),

            array(
                'name'     => 'Better Notifications Status',
                'slug'     => 'bnfw',
                'required' => false,
                'force_activation'   => false,
            ),

            array(
                'name'     => 'Nested Pages',
                'slug'     => 'wp-nested-pages',
                'required' => false,
                'force_activation'   => false,
            ),

            array(
                'name'     => 'Clean Up Optimizer',
                'slug'     => 'wp-clean-up-optimizer',
                'required' => false,
                'force_activation'   => false,
            ),

            array(
                'name'     => 'Breadcrumb NavXT',
                'slug'     => 'breadcrumb-navxt',
                'required' => false,
                'force_activation'   => false,
            ),
            array(
                'name'     => 'WooCommerce',
                'slug'     => 'woocommerce',
                'required' => false,
            ),
            array(
                'name'     => 'YITH WooCommerce Wishlist',
                'slug'     => 'yith-woocommerce-wishlist',
                'required' => false,
            ),

            array(
                'name'     => 'Admin Menu Manager (Drag N Drop)',
                'slug'     => 'admin-menu-manager',
                'required' => false,
            ),

            array(
                'name'     => 'Admin Menu Editor',
                'slug'     => 'admin-menu-editor',
                'required' => false,
            ),

            array(
                'name'     => 'Drag Drop Featured Image',
                'slug'     => 'drag-drop-featured-image',
                'required' => false,
                'force_activation'   => false,
            ),

            array(
                'name'     => 'Woocommerce Product Archive Customiser',
                'slug'     => 'woocommerce-product-archive-customiser',
                'required' => false,
                'force_activation'   => false,
            ),
            
            array(
                'name'     => 'Database Reset',
                'slug'     => 'wordpress-database-reset',
                'required' => false,
                'force_activation'   => false,
            ),

            array(
                'name'     => 'Better Delete Revision',
                'slug'     => 'better-delete-revision',
                'required' => false,
                'force_activation'   => false,
            ),

            array(
                'name'     => 'Colored Admin Post List',
                'slug'     => 'colored-admin-post-list',
                'required' => false,
                'force_activation'   => false,
            ),

            array(
                'name'     => 'Tinymce Valid Elements',
                'slug'     => 'tinymce-valid-elements',
                'required' => false,
                'force_activation'   => false,
            ),

            array(
                'name'     => 'Duplicate Menu',
                'slug'     => 'duplicate-menu',
                'required' => false,
                'force_activation'   => false,
            ),

            array(
                'name'     => 'Post Duplicator',
                'slug'     => 'post-duplicator',
                'required' => false,
                'force_activation'   => false,
            ),

            array(
                'name'     => 'Ajaxify Comments',
                'slug'     => 'wp-ajaxify-comments',
                'required' => false,
                'force_activation'   => false,
            ),

            array(
                'name'     => 'Enable Media Replace',
                'slug'     => 'enable-media-replace',
                'required' => false,
                'force_activation'   => false,
            ),

            array(
                'name'     => 'Options Optimizer',
                'slug'     => 'options-optimizer',
                'required' => false,
                'force_activation'   => false,
            ),

            array(
                'name'     => 'File Monitor',
                'slug'     => 'wordpress-file-monitor',
                'required' => false,
                'force_activation'   => false,
            ),

            array(
                'name'     => 'Login Timeout Settings',
                'slug'     => 'wp-login-timeout-settings',
                'required' => false,
                'force_activation'   => false,
            ),

            array(
                'name'     => 'Login Persistent',
                'slug'     => 'wp-persistent-login',
                'required' => false,
                'force_activation'   => false,
            ),

            array(
                'name'     => 'New User Approve',
                'slug'     => 'new-user-approve',
                'required' => false,
                'force_activation'   => false,
            ),

            array(
                'name'     => 'zM Ajax Login & Register',
                'slug'     => 'zm-ajax-login-register',
                'required' => false,
                'force_activation'   => false,
            ),

            array(
                'name'     => 'Custom Fields Permalink',
                'slug'     => 'custom-fields-permalink',
                'required' => false,
                'force_activation'   => false,
            ),

            array(
                'name'     => 'Admin Ajax Search in Backend',
                'slug'     => 'admin-ajax-search-in-backend',
                'required' => false,
                'force_activation'   => false,
            ),

            array(
                'name'     => 'Wow Html5 Animation',
                'slug'     => 'wow-html5-animation',
                'required' => false,
                'force_activation'   => false,
            ),

            array(
                'name'     => 'Rewrite',
                'slug'     => 'rewrite',
                'required' => false,
                'force_activation'   => false,
            ),

            array(
                'name'     => 'User Role Editor',
                'slug'     => 'user-role-editor',
                'required' => false,
                'force_activation'   => false,
            ),

            array(
                'name'     => 'User Switching',
                'slug'     => 'user-switching',
                'required' => false,
                'force_activation'   => false,
            ),

            array(
                'name'     => '404 Notifier',
                'slug'     => '404-notifier',
                'required' => false,
                'force_activation'   => false,
            ),

            array(
                'name'     => 'CPT Post Type Switcher',
                'slug'     => 'post-type-switcher',
                'required' => false,
                'force_activation'   => false,
            ),

            array(
                'name'     => 'CPT Custom Post Type Ui',
                'slug'     => 'custom-post-type-ui',
                'required' => false,
                'force_activation'   => false,
            ),

            array(
                'name'     => 'CPT Post Type Archive Link',
                'slug'     => 'post-type-archive-links',
                'required' => false,
                'force_activation'   => false,
            ),

            array(
                'name'     => 'Plugin Organizer',
                'slug'     => 'plugin-organizer',
                'required' => false,
                'force_activation'   => false,
            ),

            array(
                'name'     => 'Redirection',
                'slug'     => 'redirection',
                'force_activation'   => false,
                'required' => false,
            ),

            array(
                'name'     => 'WP Less',
                'slug'     => 'wp-less',
                'required' => false,
                'force_activation'   => false,
            ),

            array(
                'name'     => 'WP SCSS',
                'slug'     => 'wp-scss',
                'required' => false,
                'force_activation'   => false,
            ),
            
            array(
                'name'     => 'Widget Importer Exporter',
                'slug'     => 'widget-importer-exporter',
                'required' => false,
                'force_activation'   => false,
            ),

            array(
                'name'     => 'Widget CSS Classes',
                'slug'     => 'widget-css-classes',
                'required' => false,
                'force_activation'   => false,
            ),
            array(
                'name'     => 'CSS Javascript toolbox - JS',
                'slug'     => 'css-javascript-toolbox',
                'required' => false,
                'force_activation'   => false,
            ),




            array(
                'name'     => 'WP Pagenavi',
                'slug'     => 'wp-pagenavi',
                'required' => false,
                'force_activation'   => false,
            ),

            array(
                'name'     => 'Widget Flexible Posts',
                'slug'     => 'flexible-posts-widget',
                'required' => false,
                'force_activation'   => false,
            ),

            array(
                'name'     => 'Simple Image Sizes',
                'slug'     => 'simple-image-sizes',
                'required' => false,
                'force_activation'   => false,
            ),

            array(
                'name'     => 'SEO Local',
                'slug'     => 'wordpress-seo-local',
                'required' => false,
                'force_activation'   => false,
            ),

            array(
                'name'     => 'SEO YOAST',
                'slug'     => 'wordpress-seo',
                'required' => false,
                'force_activation'   => false,
            ),

            array(
                'name'     => 'Translate / Localization Codestyling',
                'slug'     => 'codestyling-localization',
                'required' => false,
                'force_activation'   => false,
            ),

            array(
                'name'     => 'Google Calendar Events',
                'slug'     => 'google-calendar-events',
                'required' => false,
                'force_activation'   => false,
            ),

            array(
                'name'     => 'Zalomeni',
                'slug'     => 'zalomeni',
                'required' => false,
                'force_activation'   => false,
            ),

            array(
                'name'     => 'P3 Profiler',
                'slug'     => 'p3-profiler',
                'required' => false,
                'force_activation'   => false,
            ),

            array(
                'name'     => 'Media Rename',
                'slug'     => 'media-rename',
                'required' => false,
                'force_activation'   => false,
            ),

            array(
                'name'     => 'Media File Renamer',
                'slug'     => 'media-file-renamer',
                'required' => false,
                'force_activation'   => false,
            ),

            array(
                'name'     => 'Nav Menu Roles',
                'slug'     => 'nav-menu-roles',
                'required' => false,
                'force_activation'   => false,
            ),

            array(
                'name'     => 'Admin Post Navigation',
                'slug'     => 'admin-post-navigation',
                'required' => false,
                'force_activation'   => false,
            ),

            array(
                'name'     => 'Admin Quick Jump',
                'slug'     => 'admin-quick-jump',
                'required' => false,
                'force_activation'   => false,
            ),

            array(
                'name'     => 'Taxonomy Tools',
                'slug'     => 'taxonomy-tools',
                'required' => false,
                'force_activation'   => false,
            ),

            array(
                'name'     => 'Widget Tabber Tabs',
                'slug'     => 'tabber-tabs-widget',
                'required' => false,
                'force_activation'   => false,
            ),
            array(
                'name'     => 'Advanced Excerpt',
                'slug'     => 'advanced-excerpt',
                'required' => false,
                'force_activation'   => false,
            ),

            array(
                'name'     => 'Woocommerce Grid List Toggle',
                'slug'     => 'woocommerce-grid-list-toggle',
                'required' => false,
                'force_activation'   => false,
            ),

            array(
                'name'     => 'Popular Posts',
                'slug'     => 'wordpress-popular-posts',
                'required' => false,
                'force_activation'   => false,
            ),

            array(
                'name'     => 'Clear Cache For Widgets',
                'slug'     => 'clear-cache-for-widgets',
                'required' => false,
                'force_activation'   => false,
            ),

            array(
                'name'     => 'Comment Rating',
                'slug'     => 'wp-comment-rating',
                'required' => false,
                'force_activation'   => false,
            ),

            array(
                'name'     => 'Better Internal Link Search',
                'slug'     => 'better-internal-link-search',
                'required' => false,
                'force_activation'   => false,
            ),

            array(
                'name'     => 'Three Column Screen Layout',
                'slug'     => 'three-column-screen-layout',
                'required' => false,
                'force_activation'   => false,
            ),

            array(
                'name'     => 'Super-cache',
                'slug'     => 'wp-super-cache',
                'required' => false,
                'force_activation'   => false,
            ),

            array(
                'name'     => 'Add to Any',
                'slug'     => 'add-to-any',
                'required' => false,
                'force_activation'   => false,
            ),

            array(
                'name'     => 'Widget Custom Post ',
                'slug'     => 'custom-post-widget',
                'required' => false,
                'force_activation'   => false,
            ),

            array(
                'name'     => 'Disqus Comment System',
                'slug'     => 'disqus-comment-system',
                'required' => false,
                'force_activation'   => false,
            ),

            array(
                'name'     => 'Debug Bar',
                'slug'     => 'debug-bar',
                'required' => false,
                'force_activation'   => false,
            ),

            array(
                'name'     => 'Debug Queries',
                'slug'     => 'debug-queries',
                'required' => false,
                'force_activation'   => false,
            ),

            array(
                'name'     => 'Debug Bar Actions and Filters Addon',
                'slug'     => 'debug-bar-actions-and-filters-addon',
                'required' => false,
                'force_activation'   => false,
            ),

            array(
                'name'     => 'Debug Bar Console',
                'slug'     => 'debug-bar-console',
                'required' => false,
                'force_activation'   => false,
            ),

            array(
                'name'     => 'Debug Bar Extender',
                'slug'     => 'debug-bar-extender',
                'required' => false,
                'force_activation'   => false,
            ),

            array(
                'name'     => 'Debug Bar Post Meta',
                'slug'     => 'debug-bar-post-meta',
                'required' => false,
                'force_activation'   => false,
            ),

            array(
                'name'     => 'ACF Advanced Custom Fields Snitch',
                'slug'     => 'advanced-custom-fields-field-snitch',
                'required' => false,
                'force_activation'   => false,
            ),

            array(
                'name'     => 'Shiftnav Responsive Mobile Menu',
                'slug'     => 'shiftnav-responsive-mobile-menu',
                'required' => false,
                'force_activation'   => false,
            ),

            array(
                'name'     => 'Codepress Admin Columns',
                'slug'     => 'codepress-admin-columns',
                'required' => false,
                'force_activation'   => false,
            ),

            array(
                'name'     => 'Speed Booster Pack',
                'slug'     => 'speed-booster-pack',
                'required' => false,
                'force_activation'   => false,
            ),

            array(
                'name'     => 'Subpages Extended',
                'slug'     => 'subpages-extended',
                'required' => false,
                'force_activation'   => false,
            ),

            array(
                'name'     => 'Widget Alias',
                'slug'     => 'widget-alias',
                'required' => false,
            ),
            
            array(
                'name'     => 'Reset WP Sweep',
                'slug'     => 'wp-sweep',
                'required' => false,
            ),

            array(
                'name'     => 'Wordpress File Monitor',
                'slug'     => 'wordpress-file-monitor',
                'required' => false,
            ),

            array(
                'name'     => 'Term Management Tools',
                'slug'     => 'term-management-tools',
                'required' => false,
                'force_activation'   => false,
            ),

            array(
                'name'     => 'Tabify Edit Screen',
                'slug'     => 'tabify-edit-screen',
                'required' => false,
                'force_activation'   => false,
            ),

            array(
                'name'     => 'Media From FTP',
                'slug'     => 'media-from-ftp',
                'required' => false,
                'force_activation'   => false,
            ),

            array(
                'name'     => 'Typography',
                'slug'     => 'wp-typography',
                'required' => false,
                'force_activation'   => false,
            ),

            array(
                'name'     => 'Media Search Enhanced',
                'slug'     => 'media-search-enhanced',
                'required' => false,
                'force_activation'   => false,
            ),

            array(
                'name'     => 'Codepress Admin Columns',
                'slug'     => 'codepress-admin-columns',
                'required' => false,
                'force_activation'   => false,
            ),

            array(
                'name'     => 'WP Editor',
                'slug'     => 'wp-editor',
                'required' => false,
                'force_activation'   => false,
            ),

            array(
                'name'     => 'Widget Q2W3 Fixed',
                'slug'     => 'q2w3-fixed-widget',
                'required' => false,
                'force_activation'   => false,
            ),

            array(
                'name'     => 'Contextual Help',
                'slug'     => 'sh-contextual-help',
                'required' => false,
                'force_activation'   => false,
            ),

            array(
                'name'     => 'Facebook',
                'slug'     => 'facebook',
                'required' => false,
                'force_activation'   => false,
            ),

            array(
                'name'     => 'Super Cache Clear Cache Menu',
                'slug'     => 'wp-super-cache-clear-cache-menu',
                'required' => false,
            ),
            array(
                'name'     => 'WP Store Locator',
                'slug'     => 'wp-store-locator',
                'required' => false,
            ),            

       );

        // Change this to your theme text domain, used for internationalising strings
        $theme_text_domain = 'swiftframework';

        /**
         * Array of configuration settings. Amend each line as needed.
         * If you want the default strings to be available under your own theme domain,
         * leave the strings uncommented.
         * Some of the strings are added into a sprintf, so see the comments at the
         * end of each line for what each argument will be.
         */
        $config = array(
            'domain'           => $theme_text_domain,
            // Text domain - likely want to be the same as your theme.
            
            'default_path'     => '',
            // Default absolute path to pre-packaged plugins

            'menu'             => 'install-required-plugins',
            // Menu slug
            
            'has_notices'      => false,
            // Show admin notices or not
            
            'is_automatic'     => false,
            // Automatically activate plugins after installation or not
            
            'message'          => '',
            // Message to output right before the plugins table
            
            'strings'          => array(
                'page_title'                      => __( 'Install Required Plugins', $theme_text_domain ),
                'menu_title'                      => __( 'Install Plugins', $theme_text_domain ),
                'installing'                      => __( 'Installing Plugin: %s', $theme_text_domain ),
                // %1$s = plugin name
                'oops'                            => __( 'Something went wrong with the plugin API.', $theme_text_domain ),
                'notice_can_install_required'     => _n_noop( 'This theme requires the following plugin: %1$s.', 'This theme requires the following plugins: %1$s.' ),
                // %1$s = plugin name(s)
                'notice_can_install_recommended'  => _n_noop( 'This theme recommends the following plugin: %1$s.', 'This theme recommends the following plugins: %1$s.' ),
                // %1$s = plugin name(s)
                'notice_cannot_install'           => _n_noop( 'Sorry, but you do not have the correct permissions to install the %s plugin. Contact the administrator of this site for help on getting the plugin installed.', 'Sorry, but you do not have the correct permissions to install the %s plugins. Contact the administrator of this site for help on getting the plugins installed.' ),
                // %1$s = plugin name(s)
                'notice_can_activate_required'    => _n_noop( 'The following required plugin is currently inactive: %1$s.', 'The following required plugins are currently inactive: %1$s.' ),
                // %1$s = plugin name(s)
                'notice_can_activate_recommended' => _n_noop( 'The following recommended plugin is currently inactive: %1$s.', 'The following recommended plugins are currently inactive: %1$s.' ),
                // %1$s = plugin name(s)
                'notice_cannot_activate'          => _n_noop( 'Sorry, but you do not have the correct permissions to activate the %s plugin. Contact the administrator of this site for help on getting the plugin activated.', 'Sorry, but you do not have the correct permissions to activate the %s plugins. Contact the administrator of this site for help on getting the plugins activated.' ),
                // %1$s = plugin name(s)
                'notice_ask_to_update'            => _n_noop( 'The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.', 'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.' ),
                // %1$s = plugin name(s)
                'notice_cannot_update'            => _n_noop( 'Sorry, but you do not have the correct permissions to update the %s plugin. Contact the administrator of this site for help on getting the plugin updated.', 'Sorry, but you do not have the correct permissions to update the %s plugins. Contact the administrator of this site for help on getting the plugins updated.' ),
                // %1$s = plugin name(s)
                'install_link'                    => _n_noop( 'Begin installing plugin', 'Begin installing plugins' ),
                'activate_link'                   => _n_noop( 'Activate installed plugin', 'Activate installed plugins' ),
                'return'                          => __( 'Return to Required Plugins Installer', $theme_text_domain ),
                'plugin_activated'                => __( 'Plugin activated successfully.', $theme_text_domain ),
                'complete'                        => __( 'All plugins installed and activated successfully. %s', $theme_text_domain ),
                // %1$s = dashboard link
                'nag_type'                        => 'updated'
                // Determines admin notice type - can only be 'updated' or 'error'
            )
        );

        tgmpa( $plugins, $config );

    }