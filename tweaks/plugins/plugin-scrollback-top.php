<?php

/**
*
*   Scroll Back to Top
*
**/

    function xtw_scroll_back_inline_js(){

    // echo your_plugin_symlink_fix($url);

        wp_enqueue_script('scroll_backs', XTW_URL_PATH.'/lib/js/scroll-back-to-top-min.js', array( 'jquery' ));

        $scroll_back_translation_array = array(
            'scrollDuration' => '500',
            'fadeDuration' => '0.5'
        );
        wp_localize_script( 'scroll_backs', 'scrollBackToTop', $scroll_back_translation_array );
        // wp localize nazov ako skript ktory lokalizujem

    }

    function xtw_scroll_back_html() {
        global $options;
    ?>
    <div class="scroll-back-to-top-wrapper">
        <span class="scroll-back-to-top-inner">
            <?php if ( isset( $options['opt-scroll-back-label']) ) : ?>
                <?php echo $options['opt-scroll-back-label']; ?>
            <?php endif; ?>
            <?php if( isset( $options['opt-scroll-back-icon-size'] ) && isset( $options['opt-scroll-back-icon-size'] )) : ?>
                <i class="<?php echo $options['opt-scroll-back-icon']; ?></i>
            <?php endif; ?>
        </span>
    </div>
    <?php }
    if ($options['opt-scroll-back']){
        add_action( 'wp_footer', 'xtw_scroll_back_html', 5 );
        add_action( 'wp_enqueue_scripts', 'xtw_scroll_back_inline_js' );
    }
    ?>
