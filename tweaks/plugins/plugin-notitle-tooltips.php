<?php

/**
*
* No title tooltips
*
**/

    function xtw_add_no_tttips() {
        wp_enqueue_script('notttips',  XTW_URL_PATH.'/lib/js/notttips.js', array( 'jquery' ), true);
    }

    if($options['opt-no-tttips']){
        add_action('wp_enqueue_scripts', 'xtw_add_no_tttips');
    }

?>
