<?php


/**
*
*	Rename Default Tabs ➜ Single Page
*
**/

  add_filter( 'woocommerce_product_tabs', 'xtw_woo_rename_product_tabs', 98 );

  function xtw_woo_rename_product_tabs( $rename_tabs ) {

    global $options;
    global $product;


    // Rename the Additional information tab
    if($options['opt-woo-rename-aditional-information-tab'] && $product->has_attributes()){

  	   $rename_tabs['additional_information']['title'] = $options['opt-woo-rename-aditional-information-tab'];

    }

    // Rename the Description tab
    if($options['opt-woo-rename-description-tab']){

       $rename_tabs['description']['title'] = $options['opt-woo-rename-description-tab'];

    }

    // Rename the Reviews tab

    if($options['opt-woo-rename-reviews-tab'] && comments_open()){

       $rename_tabs['reviews']['title'] = $options['opt-woo-rename-reviews-tab'];

    }
  	return $rename_tabs;

  }




/**
*
*	  Druhy nadpis (anglicky nazov) a externy link na eshop ➜ Archive Loop
*   Note - u Single page je potrebne prepisat template single-product/title.php
*
**/

    // druhy nadpis - anglicky nazov
    // function woo_subtitle() {
    //     $subtitle = get_field('alt_title');
    //     if($subtitle){
    //          echo '<h3>'.$subtitle.'</h3>';
    //     }
    //
    // }

   // global $options;

    if ( function_exists( 'get_field' ) ) {

        // add_action( 'woocommerce_after_shop_loop_item_title', 'woo_subtitle', 0 );

        if ($options['opt-woo-external-link']){
            add_action( 'woocommerce_after_shop_loop_item', 'woo_external_eshop', 15 );
        }
    }


/**
*
*	Premiestnenie tabov ➜ Single Page
*
**/

    //Removes tabs from their original location
    // remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );

    // Inserts tabs under the main right product content
    // add_action( 'woocommerce_single_product_summary', 'woocommerce_output_product_data_tabs', 60 );



/**
*
*	Vytvorenie vlastnych tabov ➜ Single Page
*
**/

if( $options['opt-woo-custom-tabs'] ){

    add_filter( 'woocommerce_product_tabs', 'xtw_theme_product_tabs' );

    function xtw_theme_product_tabs( $tabs ) {
        global $options;

      // ensure ACF is available
      if ( !function_exists( 'get_field' ) )
        return;

      $content = trim( get_field( 'product_usage' ) );
      if ( !empty( $content ) ) {
        $tabs[] = array(
          'title' => $options['opt-woo-custom-tab1'],
          'priority' => 20,
          'callback' => 'custom_woo_tab1' // zavolam funkciu
        );

    	}

      if($options['opt-woo-custom-tab2']){
        $tabs[] = array(
          'title' => $options['opt-woo-custom-tab2'],
          'priority' => 15,
          'callback' => 'custom_woo_tab2' // zavolam funkciu
        );
      }

      // if($options['opt-woo-custom-tab3']){
      //   $tabs[] = array(
      //     'title' => $options['opt-woo-custom-tab3'],
      //     'priority' => 10,
      //     'callback' => 'custom_woo_tab3' // zavolam funkciu
      //   );
      // }

      return $tabs;

      /**
      *
      *	Remove Default Tabs ➜ Single Page
      *
      **/

      add_filter( 'woocommerce_product_tabs', 'xtw_woo_remove_product_tabs', 98 );

      function xtw_woo_remove_product_tabs( $remove_tabs ) {

            global $options;
            // Remove the description tab
            // je cez custom tab

            // if(!$options['opt-woo-disable-description-tab']){
            //     unset( $tabs['description'] );
            // }

            // Remove the additional information tab
            if($options['opt-woo-disable-additional-information-tab'] == false){
                unset( $remove_tabs['additional_information'] );
            }

            // Remove the reviews tab
            if($options['opt-woo-disable-reviews-tab'] == false){
                unset( $remove_tabs['reviews'] );
            }

          return $remove_tabs;
      }

  }


/**
*
*	Obsah tabov ➜ Single Page
*
**/

    function custom_woo_tab1() {
        global $options;
      	echo '<h2>'.$options['opt-woo-custom-tab1'].'</h2>';
      	echo get_field( 'product_usage' );
    }

    function custom_woo_tab3() {
    	echo '<h2>'.$options['opt-woo-custom-tab1'].'</h2>';
    	the_content();
    }


    // function custom_woo_tab2() {
    // 	global $woocommerce, $post, $product;
    // 	echo '<h2>'.$options['opt-woo-custom-tab1'].'</h2>';
    // 	//	echo get_terms_with_desc('','ucinne_latky','<ul>','', '</ul>');
    // 	//	echo $product->list_attributes();
    // }

/**
*
*	Tabs Vlastnosti ➜ Single Page
* @todo - konfigurovatelna taxonomia (nazov....)
*
**/

    function get_terms_with_desc( $id = 0 , $taxonomy = 'ucinne_latky', $before = '', $sep = '', $after = '' ){
        $terms = get_the_terms( $id, $taxonomy );

        if ( is_wp_error( $terms ) )
                return $terms;
        if ( empty( $terms ) )
                return false;
        foreach ( $terms as $term ) {
                $link = get_term_link( $term, $taxonomy );
                if ( is_wp_error( $link ) )
                        return $link;
                $term_links[] = '<li><a href="' . esc_url( $link ) . '" rel="tag" title="'.esc_attr( $term->description ).'">' . $term->name . '</a> '. $term->description.'</li>';
        }
        $term_links = apply_filters( "term_links-$taxonomy", $term_links );
        return $before . join( $sep, $term_links ) . $after;
    }

}

?>
