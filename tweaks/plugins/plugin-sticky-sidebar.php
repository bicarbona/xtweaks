<?php

/**
*
*   Sticky Sidebar
*
**/

    function xtw_sticky_sidebar_js(){

        // wp_enqueue_script('scroll_back', plugins_url( '../lib/js/scroll-back-to-top-min.js', __FILE__ ), array( 'jquery' ));
        wp_enqueue_script('scroll_back',  XTW_URL_PATH.'/lib/js/jquery.sticky.js', array( 'jquery' ), '1', true);

        function xtw_sticky_sidebar_inline_script() {
        ?>
        <script type="text/javascript">
         jQuery(document).ready(function($){
           $("#widgetizerurmz9bs, #widgetizerun5vcqe").sticky({topSpacing: 0});
         });
        </script>
        <?php

        }
        add_action( 'wp_footer', 'print_my_inline_script' );

    }
    add_action( 'wp_enqueue_scripts', 'xtw_sticky_sidebar_js' );

?>
