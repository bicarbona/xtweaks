<?php

// if (!is_admin()) {

/**
*
* Slick Slider
*
**/

    function xtw_add_slick_script() {
        wp_enqueue_script('slick-script',  XTW_URL_PATH.'lib/js/slick.min.js', 1, array( 'jquery' ), true);
    }

    if($options['opt-slick-slider-js']){
        add_action('wp_enqueue_scripts', 'xtw_add_slick_script');
    }

/**
*
* Responsive Tabs
* https://github.com/jellekralt/Responsive-Tabs
**/

    function xtw_add_responsive_tabs_script() {
        wp_enqueue_script('responsive-tabs-script', XTW_URL_PATH.'lib/js/jquery.responsiveTabs.min.js', 1, array( 'jquery' ), true);
    }

    if($options['opt-responsive-tabs-components']){
        add_action('wp_enqueue_scripts', 'xtw_add_responsive_tabs_script');
    }

/**
*
* typewriter
* https://github.com/benrlodge/typewriter
**/

    function xtw_add_typer_script() {
        wp_enqueue_script('typer', XTW_URL_PATH.'lib/js/jquery.typer-min.js', 1, array( 'jquery' ), true);
    }

    if($options['opt-typer-animation-text']){
        add_action('wp_enqueue_scripts', 'xtw_add_typer_script');
    }


    /**
    *
    * typewriter
    * https://github.com/benrlodge/typewriter
    **/

    function xtw_add_typed_script() {
        wp_enqueue_script('typer', XTW_URL_PATH.'lib/js/typed.min.js', 1, array( 'jquery' ), true);
    }

    if($options['opt-typed-animation-text']){
        add_action('wp_enqueue_scripts', 'xtw_add_typed_script');
    }



    /**
    *
    * filterizr
    * https://github.com/benrlodge/typewriter
    **/

    function xtw_add_filterizr_grid_script() {
        wp_enqueue_script('filterizr', XTW_URL_PATH.'lib/js/jquery.filterizr.js', 1, array( 'jquery' ), false);
    }

    if( $options['opt-filterizr-grid'] ){
        add_action('wp_enqueue_scripts', 'xtw_add_filterizr_grid_script');
    }





?>
