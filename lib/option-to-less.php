<?php

global $post;

// print_r($post);
// echo $publishview = publish_view_enqueue();
$publishview = true;

//add_less_var( 'brandcolour', '#95501d' ); // add single variable

/**
Variables
**/

add_filter( 'less_vars', 'register_less_vars', 10, 1 ); // Global
add_filter( 'pless_vars', 'register_less_vars', 10, 1 ); // Pagelines VARS

function register_less_vars($vars){

// Redux
$options = get_option('redux_tweaks');

// Paths / urls
$vars['plugin_dir'] = '"'.XTW_PLUGIN_PATH.'"';
$vars['plugin_url'] = '"'.plugin_dir_url( __FILE__).'less'.'"';
// $vars['url_admin_logo'] = '"'.XTW_PLUGIN_PATH.'"';

// $vars['url_admin_logo'] = '"'.$url_admin_logo.'"';
// $vars['color_firm'] = 'red';

/**
Include LESS module
**/

// Icons
$vars['add-arrow-icons'] = $options['opt-arrow-icons'] ? $options['opt-arrow-icons']: 0;
$vars['add-feather-icons'] = $options['opt-feather-icons'] ? $options['opt-feather-icons']: 0;
$vars['add-ss-gizmo'] = $options['opt-less-ss-gizmo'] ? $options['opt-less-ss-gizmo']: 0;

/**
 Plugins
**/
$vars['add-plugins'] = $options['opt-less-plugins'] ? $options['opt-less-plugins']: 0;
$vars['add-wp-pagenavi'] = $options['opt-less-wp-pagenavi'] ? $options['opt-less-wp-pagenavi']: 0;
$vars['add-woocommerce'] = $options['opt-less-woocommerce'] ? $options['opt-less-woocommerce']: 0;
$vars['add-scroll-back'] = $options['opt-scroll-back'] ? $options['opt-scroll-back']: 0;
$vars['add-colorbox'] = $options['opt-less-colorbox'] ? $options['opt-less-colorbox']: 0;
$vars['add-fixed-sidebar'] = $options['opt-sidebar-fixed'] ? $options['opt-sidebar-fixed']: 0;

$vars['add-responsive-tabs'] = $options['opt-responsive-tabs-components'] ? $options['opt-responsive-tabs-components']: 0;




// Components
$vars['add-buttons'] = $options['opt-less-buttons'] ? $options['opt-less-buttons']: 0;
$vars['add-wrappers'] = $options['opt-less-wrappers'] ? $options['opt-less-wrappers']: 0;
$vars['add-animations'] = $options['opt-less-animations'] ? $options['opt-less-animations']: 0;
$vars['add-labels-badges'] = $options['opt-less-labels-badges'] ? $options['opt-less-labels-badges']: 0;
$vars['add-alert-higlight'] = $options['opt-less-alert-higlight'] ? $options['opt-less-alert-higlight']: 0;

// Webflow Grid
$vars['add-webflow-grid'] = $options['opt-less-webflow-grid'] ? $options['opt-less-webflow-grid'] : 0;

// Fixes
$vars['add-fix-headway'] = $options['opt-less-add-fix-headway'] ? $options['opt-less-add-fix-headway']: 0;

// 'add-typography'] ? $optionsopt-['opt-less-typography']: 'DEFAULT';
// $vars['test'] = $options['test'] ? strip_tags($options['opt-shiftnav-hide-menu-element']): 'DEFAULT';

/**
 Shiftnav
**/
$vars['opt-shiftnav-togglebar-background'] = $options['opt-shiftnav-togglebar-background']['color'] ? $options['opt-shiftnav-togglebar-background']['color']: 'DEFAULT';
$vars['opt-shiftnav-togglebar-text-color'] = $options['opt-shiftnav-togglebar-text-color']['color'] ? $options['opt-shiftnav-togglebar-text-color']['color']: 'DEFAULT';
$vars['opt-shiftnav-togglebar-breakpoint'] = $options['opt-shiftnav-togglebar-breakpoint'] ? $options['opt-shiftnav-togglebar-breakpoint'].'px': '600px';
$vars['opt-shiftnav-hide-menu'] = $options['opt-shiftnav-hide-menu'] ? $options['opt-shiftnav-hide-menu']: 'DEFAULT';
// 'opt-shiftnav-hide-menu-element'] ? $options['opt-shiftnav-hide-menu-element']: 'DEFAULT';

/**
 Woocommerce Cart
**/

$vars['opt-woo-cart-typo-font-size'] = $options['opt-woo-cart-typo']['font-size'] ? $options['opt-woo-cart-typo']['font-size']: 'DEFAULT';
$vars['opt-woo-cart-typo-line-height'] = $options['opt-woo-cart-typo']['line-height'] ? $options['opt-woo-cart-typo']['line-height']: 'DEFAULT';

$vars['opt-woo-cart-color-regular'] = $options['opt-woo-cart-color']['regular'] ? $options['opt-woo-cart-color']['regular']: 'DEFAULT';
$vars['opt-woo-cart-color-hover'] = $options['opt-woo-cart-color']['hover'] ? $options['opt-woo-cart-color']['hover']: 'DEFAULT';

// Backopt-ground
$vars['opt-woo-cart-background-regular'] = $options['opt-woo-cart-background']['regular'] ? $options['opt-woo-cart-background']['regular']: 'DEFAULT';
$vars['opt-woo-cart-background-hover'] = $options['opt-woo-cart-background']['hover'] ? $options['opt-woo-cart-background']['hover']: 'DEFAULT';

$vars['opt-woo-cart-padding-top'] = $options['opt-woo-cart-padding']['padding-top'] ? $options['opt-woo-cart-padding']['padding-top']: 'DEFAULT';
$vars['opt-woo-cart-padding-bottom'] = $options['opt-woo-cart-padding']['padding-bottom'] ? $options['opt-woo-cart-padding']['padding-bottom']: 'DEFAULT';
$vars['opt-woo-cart-padding-left'] = $options['opt-woo-cart-padding']['padding-left'] ? $options['opt-woo-cart-padding']['padding-left']: 'DEFAULT';
$vars['opt-woo-cart-padding-right'] = $options['opt-woo-cart-padding']['padding-right'] ? $options['opt-woo-cart-padding']['padding-right']: 'DEFAULT';

// Row Border
$vars['opt-woo-cart-row-border-color'] = $options['opt-woo-cart-row-border-color'] ? $options['opt-woo-cart-row-border-color']: 'DEFAULT';

// Btn
$vars['opt-woo-cart-btn-width'] = $options['opt-woo-cart-btn-width'] ? $options['opt-woo-cart-btn-width']: 'DEFAULT';
$vars['opt-woo-cart-btn-padding-top'] = $options['opt-woo-cart-btn-padding']['padding-top'] ? $options['opt-woo-cart-btn-padding']['padding-top']: 'DEFAULT';
$vars['opt-woo-cart-btn-padding-bottom'] = $options['opt-woo-cart-btn-padding']['padding-bottom'] ? $options['opt-woo-cart-btn-padding']['padding-bottom']: 'DEFAULT';
$vars['opt-woo-cart-btn-padding-right'] = $options['opt-woo-cart-btn-padding']['padding-right'] ? $options['opt-woo-cart-btn-padding']['padding-right']: 'DEFAULT';
$vars['opt-woo-cart-btn-padding-left'] = $options['opt-woo-cart-btn-padding']['padding-left'] ? $options['opt-woo-cart-btn-padding']['padding-left']: 'DEFAULT';

// Btn Typo
$vars['opt-woo-cart-btn-typo-font-size'] =  $options['opt-woo-cart-btn-typo']['font-size'] ? $options['opt-woo-cart-btn-typo']['font-size']: 'DEFAULT';
$vars['opt-woo-cart-btn-typo-line-height'] = $options['opt-woo-cart-btn-typo']['line-height'] ? $options['opt-woo-cart-btn-typo']['line-height']: 'DEFAULT';

// View Cart
$vars['opt-woo-cart-btn-view-background-regular'] = $options['opt-woo-cart-btn-view-background']['regular'] ? $options['opt-woo-cart-btn-view-background']['regular']: 'DEFAULT';
$vars['opt-woo-cart-btn-view-background-hover'] = $options['opt-woo-cart-btn-view-background']['hover'] ? $options['opt-woo-cart-btn-view-background']['hover']: 'DEFAULT';
$vars['opt-woo-cart-btn-view-color-regular'] = $options['opt-woo-cart-btn-view-color']['regular'] ? $options['opt-woo-cart-btn-view-color']['regular']: 'DEFAULT';
$vars['opt-woo-cart-btn-view-color-hover'] = $options['opt-woo-cart-btn-view-color']['hover'] ? $options['opt-woo-cart-btn-view-color']['hover']: 'DEFAULT';

// Margin
$vars['opt-woo-cart-btn-view-margin-bottom'] = $options['opt-woo-cart-btn-view-margin']['padding-bottom'] ? $options['opt-woo-cart-btn-view-margin']['padding-bottom']: 'DEFAULT';

// Checkout
$vars['opt-woo-cart-btn-checkout-background-regular'] = $options['opt-woo-cart-btn-checkout-background']['regular'] ? $options['opt-woo-cart-btn-checkout-background']['regular']: 'DEFAULT';
$vars['opt-woo-cart-btn-checkout-background-hover'] = $options['opt-woo-cart-btn-checkout-background']['hover'] ? $options['opt-woo-cart-btn-checkout-background']['hover']: 'DEFAULT';

$vars['opt-woo-cart-btn-checkout-color-regular'] = $options['opt-woo-cart-btn-checkout-color']['regular'] ? $options['opt-woo-cart-btn-checkout-color']['regular']: 'DEFAULT';
$vars['opt-woo-cart-btn-checkout-color-hover'] = $options['opt-woo-cart-btn-checkout-color']['hover'] ? $options['opt-woo-cart-btn-checkout-color']['hover']: 'DEFAULT';

// Content
$vars['opt-woo-cart-content-background'] = $options['opt-woo-cart-content-background'] ? $options['opt-woo-cart-content-background']: 'DEFAULT';
$vars['opt-woo-cart-content-typo-font-size'] = $options['opt-woo-cart-content-typo']['font-size'] ? $options['opt-woo-cart-content-typo']['font-size']: 'DEFAULT';
$vars['opt-woo-cart-content-typo-text-align'] = $options['opt-woo-cart-content-typo']['text-align'] ? $options['opt-woo-cart-content-typo']['text-align']: 'DEFAULT';

// Padding
$vars['opt-woo-cart-content-padding-top'] = $options['opt-woo-cart-content-padding']['padding-top'] ? $options['opt-woo-cart-content-padding']['padding-top']: 'DEFAULT';
$vars['opt-woo-cart-content-padding-bottom'] = $options['opt-woo-cart-content-padding']['padding-bottom'] ? $options['opt-woo-cart-content-padding']['padding-bottom']: 'DEFAULT';
$vars['opt-woo-cart-content-padding-left'] = $options['opt-woo-cart-content-padding']['padding-left'] ? $options['opt-woo-cart-content-padding']['padding-left']: 'DEFAULT';
$vars['opt-woo-cart-content-padding-right'] = $options['opt-woo-cart-content-padding']['padding-right'] ? $options['opt-woo-cart-content-padding']['padding-right']: 'DEFAULT';

// Remove x
$vars['opt-woo-cart-remove-color-regular'] = $options['opt-woo-cart-remove-color']['regular'] ? $options['opt-woo-cart-remove-color']['regular']: 'DEFAULT';
$vars['opt-woo-cart-remove-color-hover'] =$options['opt-woo-cart-remove-color']['hover'] ? $options['opt-woo-cart-remove-color']['hover']: 'DEFAULT';

$vars['opt-woo-cart-remove-typo-font-size'] = $options['opt-woo-cart-remove-typo']['font-size'] ? $options['opt-woo-cart-remove-typo']['font-size']: 'DEFAULT';
$vars['opt-woo-cart-remove-typo-line-height'] = $options['opt-woo-cart-remove-typo']['line-height'] ? $options['opt-woo-cart-remove-typo']['line-height']: 'DEFAULT';

// Item Title
$vars['opt-woo-cart-item-title-typo-font-size'] = $options['opt-woo-cart-item-title-typo']['font-size'] ? $options['opt-woo-cart-item-title-typo']['font-size']: 'DEFAULT';
$vars['opt-woo-cart-item-title-typo-line-height'] = $options['opt-woo-cart-item-title-typo']['line-height'] ? $options['opt-woo-cart-item-title-typo']['line-height']: 'DEFAULT';

$vars['opt-woo-cart-item-title-color-regular'] = $options['opt-woo-cart-item-title-color']['regular'] ? $options['opt-woo-cart-item-title-color']['regular']: 'DEFAULT';
$vars['opt-woo-cart-item-title-color-hover'] = $options['opt-woo-cart-item-title-color']['hover'] ? $options['opt-woo-cart-item-title-color']['hover']: 'DEFAULT';

// Image
$vars['opt-woo-cart-image-size'] = $options['opt-woo-cart-image-size'] ? $options['opt-woo-cart-image-size'].'px': 'DEFAULT';

// Fixed Position
$vars['opt-woo-cart-fixed-margin-left'] = $options['opt-woo-cart-fixed-margin']['margin-left'] ? $options['opt-woo-cart-fixed-margin']['margin-left']: 'DEFAULT';
$vars['opt-woo-cart-fixed-margin-right'] = $options['opt-woo-cart-fixed-margin']['margin-right'] ? $options['opt-woo-cart-fixed-margin']['margin-right']: 'DEFAULT';
$vars['opt-woo-cart-fixed-margin-top'] = $options['opt-woo-cart-fixed-margin']['margin-top'] ? $options['opt-woo-cart-fixed-margin']['margin-top']: 'DEFAULT';
$vars['opt-woo-cart-fixed-margin-bottom'] = $options['opt-woo-cart-fixed-margin']['margin-bottom'] ? $options['opt-woo-cart-fixed-margin']['margin-bottom']: 'DEFAULT';

// Subtotal
$vars['opt-woo-cart-subtotal-background'] = $options['opt-woo-cart-subtotal-background'] ? $options['opt-woo-cart-subtotal-background']: 'DEFAULT';
$vars['opt-woo-cart-subtotal-color'] = $options['opt-woo-cart-subtotal-color'] ? $options['opt-woo-cart-subtotal-color']: 'DEFAULT';
$vars['opt-woo-cart-subtotal-typo-text-align'] = $options['opt-woo-cart-subtotal-typo']['text-align'] ? $options['opt-woo-cart-subtotal-typo']['text-align']: 'DEFAULT';
$vars['opt-woo-cart-subtotal-typo-font-size'] = $options['opt-woo-cart-subtotal-typo']['font-size'] ? $options['opt-woo-cart-subtotal-typo']['font-size']: 'DEFAULT';
$vars['opt-woo-cart-subtotal-typo-line-height'] = $options['opt-woo-cart-subtotal-typo']['line-height'] ? $options['opt-woo-cart-subtotal-typo']['line-height']: 'DEFAULT';

/**
Fixed Sidebar
**/
$vars['opt-sidebar-fixed-background-color'] = $options['opt-sidebar-fixed-background']['color'] ? $options['opt-sidebar-fixed-background']['color']: 'DEFAULT';
$vars['opt-sidebar-fixed-background-rgba'] = $options['opt-sidebar-fixed-background']['alpha'] ? $options['opt-sidebar-fixed-background']['alpha']: 'DEFAULT';

$vars['opt-sidebar-fixed-zindex'] = $options['opt-sidebar-fixed-zindex'] ? $options['opt-sidebar-fixed-zindex']: '8';
$vars['opt-sidebar-fixed-width'] = $options['opt-sidebar-fixed-width'] ? $options['opt-sidebar-fixed-width']['width']: 'DEFAULT';
$vars['opt-sidebar-fixed-height'] = $options['opt-sidebar-fixed-height'] ? $options['opt-sidebar-fixed-height']['height']: 'DEFAULT';

$vars['opt-sidebar-fixed-transition'] = $options['opt-sidebar-fixed-transition'] ? $options['opt-sidebar-fixed-transition']: 'DEFAULT';
// $vars['baseColor'] = $options['opt-sidebar-fixed-transition'] ? $options['opt-sidebar-fixed-transition']: 'DEFAULT';

$vars['opt-sidebar-fixed-transform-x'] = $options['opt-sidebar-fixed-transform']['width'] ? $options['opt-sidebar-fixed-transform']['width']: 'DEFAULT';
$vars['opt-sidebar-fixed-transform-y'] = $options['opt-sidebar-fixed-transform']['height'] ? $options['opt-sidebar-fixed-transform']['height']: 'DEFAULT';
$vars['opt-sidebar-fixed-id'] = $options['opt-sidebar-fixed-id'] ? $options['opt-sidebar-fixed-id']: 'DEFAULT';

/**
Fix HW Full Width Wrapper
**/
$vars['opt-hw-fullwidth-wrapper'] = $options['opt-hw-fullwidth-wrapper'] ? $options['opt-hw-fullwidth-wrapper']: 'DEFAULT';
$vars['opt-hw-fullwidth-wrapper-id'] = $options['opt-hw-fullwidth-wrapper-id'] ? $options['opt-hw-fullwidth-wrapper-id']: 'DEFAULT';

/**
Scroll Back to top
**/
$vars['opt-scroll-back-hover-background'] = $options['opt-scroll-back-hover-background']['color'] ? $options['opt-scroll-back-hover-background']['color']: 'DEFAULT';
$vars['opt-scroll-back-hover'] = $options['opt-scroll-back-hover']['color'] ? $options['opt-scroll-back-hover']['color']: 'DEFAULT';
// $vars['baseColor'] = $options['opt-scroll-back-color']['color'] ? $options['opt-scroll-back-color']['color']: 'DEFAULT';

$vars['opt-scroll-back-dimension-width'] = $options['opt-scroll-back-dimension']['width'] ? $options['opt-scroll-back-dimension']['width']: 'DEFAULT';
$vars['opt-scroll-back-dimension-height'] =  $options['opt-scroll-back-dimension']['height'] ? $options['opt-scroll-back-dimension']['height']: 'DEFAULT';

$vars['opt-scroll-back-border-radius'] = $options['opt-scroll-back-border-radius'] ? $options['opt-scroll-back-border-radius'].'px': 'DEFAULT';
$vars['opt-scroll-back-background'] = $options['opt-scroll-back-background']['color'] ? $options['opt-scroll-back-background']['color']: 'DEFAULT';
$vars['opt-scroll-back-horizontal-distance'] = $options['opt-scroll-back-horizontal-distance'] ? $options['opt-scroll-back-horizontal-distance'].'px': 'DEFAULT';
$vars['opt-scroll-back-vertical-distance'] = $options['opt-scroll-back-vertical-distance'] ? $options['opt-scroll-back-horizontal-distance'].'px': 'DEFAULT';

$vars['opt-scroll-back-border-color'] = $options['opt-scroll-back-border']['border-color'] ? $options['opt-scroll-back-border']['border-color']: 'DEFAULT';
$vars['opt-scroll-back-border-style'] = $options['opt-scroll-back-border']['border-style'] ? $options['opt-scroll-back-border']['border-style']: 'DEFAULT';
$vars['opt-scroll-back-border-width'] = $options['opt-scroll-back-border']['border-top'] ? $options['opt-scroll-back-border']['border-top']: 'DEFAULT';

$vars['opt-scroll-back-font-size'] = $options['opt-scroll-back-font-size'] ? $options['opt-scroll-back-font-size'].'px': 'DEFAULT';
$vars['opt-scroll-back-line-height'] = $options['opt-scroll-back-line-height'] ? $options['opt-scroll-back-line-height'].'px': 'DEFAULT';

// 'opt-scroll-back-border-top'] ? $options['opt-scroll-back-border']['border-top']: 'DEFAULT';
// 'opt-scroll-back-border-bottom'] ? $options['opt-scroll-back-border']['border-bottom']: 'DEFAULT';
// 'opt-scroll-back-border-right'] ? $options['opt-scroll-back-border']['border-right']: 'DEFAULT';
// 'opt-scroll-back-border-left'] ? $options['opt-scroll-back-border']['border-left']: 'DEFAULT';
// opt-scroll-back-hover-background

// Min Height - HW??
$vars['opt-disable-min-height'] = $options['opt-less-disable-min-height'] ? $options['opt-less-disable-min-height']: true;

/**
Define Global Colors
**/
$vars['opt-color-firm'] = $options['opt-color-firm']['color'] ? $options['opt-color-firm']['color']: 'DEFAULT';
$vars['opt-color-firm-transparency'] = $options['opt-color-firm']['alpha'] ? $options['opt-color-firm']['alpha']: 'DEFAULT';

$vars['opt-color-red'] = $options['opt-color-red']['color'] ? $options['opt-color-red']['color']: 'DEFAULT';
$vars['opt-color-red-transparency'] = $options['opt-color-red']['alpha'] ? $options['opt-color-red']['alpha']: 'DEFAULT';

$vars['opt-color-blue'] = $options['opt-color-blue']['color'] ? $options['opt-color-blue']['color']: 'DEFAULT';
$vars['opt-color-blue-transparency'] = $options['opt-color-blue']['alpha'] ? $options['opt-color-blue']['alpha']: 'DEFAULT';

$vars['opt-color-orange'] = $options['opt-color-orange']['color'] ? $options['opt-color-orange']['color']: 'DEFAULT';
$vars['opt-color-orange-transparency'] = $options['opt-color-orange']['alpha'] ? $options['opt-color-orange']['alpha']: 'DEFAULT';

$vars['opt-color-yellow'] = $options['opt-color-yellow']['color'] ? $options['opt-color-yellow']['color']: 'DEFAULT';
$vars['opt-color-yellow-transparency'] = $options['opt-color-yellow']['alpha'] ? $options['opt-color-yellow']['alpha']: 'DEFAULT';

$vars['opt-color-violet'] = $options['opt-color-violet']['color'] ? $options['opt-color-violet']['color']: 'DEFAULT';
$vars['opt-color-violet-transparency'] = $options['opt-color-violet']['alpha'] ? $options['opt-color-violet']['alpha']: 'DEFAULT';

$vars['opt-color-black'] = $options['opt-color-black']['color'] ? $options['opt-color-black']['color']: 'DEFAULT';
$vars['opt-color-black-transparency'] = $options['opt-color-black']['alpha'] ? $options['opt-color-black']['alpha']: 'DEFAULT';

$vars['opt-color-white'] = $options['opt-color-white']['color'] ? $options['opt-color-white']['color']: 'DEFAULT';
$vars['opt-color-white-transparency'] = $options['opt-color-white']['alpha'] ? $options['opt-color-white']['alpha']: 'DEFAULT';

$vars['opt-color-success'] = $options['opt-color-success']['color'] ? $options['opt-color-success']['color']: 'DEFAULT';
$vars['opt-color-success-transparency'] = $options['opt-color-success']['alpha'] ? $options['opt-color-success']['alpha']: 'DEFAULT';

$vars['opt-olor-danger'] = $options['opt-color-danger']['color'] ? $options['opt-color-danger']['color']: 'DEFAULT';
$vars['opt-color-danger-transparency'] = $options['opt-color-danger']['alpha'] ? $options['opt-color-danger']['alpha']: 'DEFAULT';

$vars['opt-color-info'] = $options['opt-color-info']['color'] ? $options['opt-color-info']['color']: 'DEFAULT';
$vars['opt-color-info-transparency'] = $options['opt-color-info']['alpha'] ? $options['opt-color-info']['alpha']: 'DEFAULT';

$vars['opt-color-warning'] = $options['opt-color-warning']['color'] ? $options['opt-color-warning']['color']: 'DEFAULT';
$vars['opt-color-warning-transparency'] = $options['opt-color-warning']['alpha'] ? $options['opt-color-warning']['alpha']: 'DEFAULT';

$vars['opt-color-inverse'] = $options['opt-color-inverse']['color'] ? $options['opt-color-inverse']['color']: 'DEFAULT';
$vars['opt-color-inverse-transparency'] = $options['opt-color-inverse']['alpha'] ? $options['opt-color-inverse']['alpha']: 'DEFAULT';

$vars['opt-color-base'] = $options['opt-color-base']['color'] ? $options['opt-color-base']['color']: 'DEFAULT';
$vars['opt-color-base-transparency'] = $options['opt-color-base']['alpha'] ? $options['opt-color-base']['alpha']: 'DEFAULT';

$vars['opt-color-gray'] = $options['opt-color-gray']['color'] ? $options['opt-color-gray']['color']: 'DEFAULT';
$vars['opt-color-gray-transparency'] = $options['opt-color-gray']['alpha'] ? $options['opt-color-gray']['alpha']: 'DEFAULT';

$vars['opt-color-green'] = $options['opt-color-green']['color'] ? $options['opt-color-green']['color'] : 'DEFAULT';
$vars['opt-color-green-transparency'] = $options['opt-color-green']['alpha'] ? $options['opt-color-green']['alpha']: 'DEFAULT';

/**
Admin / Login
**/
$vars['opt-admin-login-logo'] = $options['opt-admin-login-logo'] ? $options['opt-admin-login-logo'] : 'DEFAULT';
$vars['opt-admin-login-body-background'] = $options['opt-admin-login-body-background']['background-color'] ? $options['opt-admin-login-body-background']['background-color']: '#ffffff';

// 'opt-admin-login-form-background'] ? $options['opt-admin-login-form-background']['color']: 'DEFAULT';
// 'opt-admin-login-color-link-text'] ? $options['opt-admin-login-color-link-text']['regular']: 'DEFAULT';
$vars['opt-admin-login-color-link-text-hover'] = $options['opt-admin-login-color-link-text']['hover'] ? $options['opt-admin-login-color-link-text']['hover']: 'DEFAULT';

// 'opt-color-firm'] ? $options['opt-color-firm']['color']: 'DEFAULT';
// 'opt-color-firm-transparency'] ? $options['opt-color-firm']['alpha']: 'DEFAULT';

/**
Admin Color
**/
$vars['opt-admin-highlight-color'] = $options['opt-admin-highlight-color']['color'] ? $options['opt-admin-highlight-color']['color']: '#251045';
$vars['opt-admin-base-color'] = $options['opt-admin-base-color']['color'] ? $options['opt-admin-base-color']['color']: '#c3b752';
$vars['opt-admin-notification-color'] = $options['opt-admin-notification-color']['color'] ? $options['opt-admin-notification-color']['color']: '#f295d6';
$vars['opt-admin-body-background'] = $options['opt-admin-body-background']['color'] ? $options['opt-admin-body-background']['color']: '#344aaa';
$vars['opt-admin-link-color'] = $options['opt-admin-link-color']['color'] ? $options['opt-admin-link-color']['color']: '#75c8c7';

/**
Admin Tweaks
**/
// $vars['add-publish-view'] = $options['opt-add-publish-view'] ? $options['opt-add-publish-view']: true; // tlacitko na stranke postu / page
$vars['fix-acf-label'] = $options['opt-fix-acf-label'] ? $options['opt-fix-acf-label']: 'DEFAULT';

/**
Sticky menu
**/
// 'add-sticky-menu'] ? $options['opt-sticky-menu']: 'DEFAULT';
//     'opt-sticky-zindex'   ? $options['opt-sticky-zindex']: 'DEFAULT';
//     'opt-sticky-active-on-height'   ? $options['opt-sticky-active-on-height']: 'DEFAULT';
//     'opt-sticky-transition-time'   ? $options['opt-sticky-transition-time'].'s': 'DEFAULT';
//     'opt-sticky-disable-small-screen'   ? $options['opt-sticky-disable-small-screen'].'px': 'DEFAULT';
//     'opt-sticky-background'   ? $options['opt-sticky-background']['color']: 'DEFAULT';

  return $vars;
}


?>
