jQuery( document ).ready( function($) {
	
	$(document).on( 'click', '.delete-post', function() {
		var id = $(this).data('id');
		var nonce = $(this).data('nonce');
		var post = $(this).parents('.post:first');
		var action = $(this).data('action');
		var status = $(this).data('status');

		$.ajax({
			type: 'get',
			url: MyAjax.ajaxurl,
			data: {
				action: action,
				nonce: nonce,
				status: status,
				id: id
			},
			success: function( result ) {
				if( result == 'success' ) {
					post.fadeOut( function(){
						post.remove();
					});
				}
			}
		})
		return false;
	})

	console.log(MyAjax.ajaxurl);

	// Sortable
	//http://api.jqueryui.com/sortable/#option-items
	$( ".loop" ).sortable({
		scroll: true,
		axis: 'y',
        opacity: 0.94,
        revert: true,
        cursor: 'move',
        // handle: '.hndle' //element that can initiate the sort
    });

    $( ".loop" ).disableSelection();

});
