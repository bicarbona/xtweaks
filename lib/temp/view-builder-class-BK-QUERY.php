<?php
require_once( ABSPATH . 'wp-admin/includes/plugin.php' );

// $plugin_file = '/Users/gentleman/Dropbox/Server/wp-projects/option/wp-content/themes/twentysixteen/bj-lazy-load.php';
//
// $sule = get_plugin_data( $plugin_file, $markup = true, $translate = true );
//
// echo '<pre>';
// print $sule['Name'] ."<br>";
// print $sule['Description'];
// echo '</pre>';
//


// print_r($sule);
class view_builder {

  // private $query_generator;
  public $id_vb_view;
  public $single_vb_view = true;

  function __construct($id_vb_view){

    $this->id_vb_view = $id_vb_view;

    $this->vb_view = get_post( $this->id_vb_view );
    wp_reset_postdata();

  }

  public function vb_redux() {
    // print  $this->query_generator;
    $this->title_view = $this->vb_view->post_title;

    /**
    *
    * vb-view Redux Options
    *
    **/

    $this->id_vb_query = redux_post_meta( "redux_tweaks", $this->vb_view->ID, "vb-query-view");
    $this->query_template = redux_post_meta( "redux_tweaks", $this->vb_view->ID, "vb-template");

    $this->columns_md = redux_post_meta( "redux_tweaks", $this->vb_view->ID, "vb-columns-md");
    $this->columns_lg = redux_post_meta( "redux_tweaks", $this->vb_view->ID, "vb-columns-lg");
    $this->id_vb_query = redux_post_meta( "redux_tweaks", $this->vb_view->ID, "vb-query-view");
    $this->featured_image = redux_post_meta( "redux_tweaks", $this->vb_view->ID, "vb-featured-image");


    /**
    VB-Query Args
    **/
    $this->vb_query = get_post( $this->id_vb_query );

    $this->vb_query->post_title;

    $this->post_type = redux_post_meta( "redux_tweaks", $this->vb_query->ID, "vb-post-type");
    $this->post_status = redux_post_meta( "redux_tweaks", $this->vb_query->ID, "vb-post-status");

    $this->posts_per_page = redux_post_meta( "redux_tweaks", $this->vb_query->ID, "vb-posts-per-page");

    $this->orderby = redux_post_meta( "redux_tweaks", $this->vb_query->ID, "vb-orderby");
    $this->order = redux_post_meta( "redux_tweaks", $this->vb_query->ID, "vb-order");

    $this->offset = redux_post_meta( "redux_tweaks", $this->vb_query->ID, "vb-skip");

    $this->author_id = redux_post_meta( "redux_tweaks", $this->vb_query->ID, "vb-authors");

    $this->ignore_sticky_posts = redux_post_meta( "redux_tweaks", $this->vb_query->ID, "vb-sticky");
    $this->category__in = redux_post_meta( "redux_tweaks", $this->vb_query->ID, "vb-inc-cats");


    //* Temp single taxonomy*//

    $this->taxonomy = redux_post_meta( "redux_tweaks", $this->vb_query->ID, "vb-other-tax");

    $this->term_slug = redux_post_meta( "redux_tweaks", $this->vb_query->ID, "vb-other-tax-term");
    // print_r($this->term_id);
  }

  public function vb_debug( $vb_debug = true ) {
  	/**
  	*
  	*		Debug purpose
  	*		@return Redux Options
  	*
  	**/

    $this->vb_debug = $vb_debug;

  	if ( $vb_debug ) {
  			echo '<pre>';
  			echo '<h3>View Name</h3>' .$this->title_view. '<br>';
  			echo '</pre>';

  			echo '<pre>';
  			echo '<h3>Columns MD</h3>' .$this->columns_md. '<br>';
  			echo '<h3>Columns LG</h3>' .$this->columns_lg. '<br>';
  			echo '</pre>';

  			echo '<pre>';
  			echo '<h3>VB Query ID</h3>' . $this->id_vb_query . '<br>';
  			echo '</pre>';

  			echo '<pre>';
  			echo '<h3>Template File Name</h3>' .$this->query_template;
  			echo '</pre>';

  			// Redux all Query Options
  			$this->all_vb_query_options = redux_post_meta( "redux_tweaks", $this->vb_query->ID, "");

  			echo '<pre>';
  			echo '<h3>Redux All Query Options </h3>';
  			print_r($this->all_vb_query_options);
  			echo '</pre>';

  			// Redux all View Options
  			$this->all_vb_view_options = redux_post_meta( "redux_tweaks", $this->vb_view->ID, "");

  			echo '<pre>';
  			echo '<h3>Redux All View Options </h3>';
  			print_r($this->all_vb_view_options);
  			echo '</pre>';

  	/**
  	*
  	*		Debug purpose
  	*		@return Query args
  	*
  	**/

  		  echo '<pre>';
  			echo '<h3>Query args</h3>';
  		  print_r($this->args);
  		  echo '</pre>';
  			echo '<pre>';
  			echo '<h3>View Temlate path</h3>';
  			print( XTW_PLUGIN_PATH .'templates/'.$this->query_template.'/'. $this->query_template.'.php' );
  			echo '</pre>';
  	}

  }

/**
*
*   vb_query
*   @return wp_query
**/

public function vb_query() {
  	/**
  	*
  	* 	Setup Query args
  	*		@return $args for WP_Query
  	*
  	**/

  			$this->paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

  			$this->args = array(
  				'post_type'             => $this->post_type,
  				'offset'                => $this->offset,
  				'order'                 => $this->order,
  				'orderby'               => $this->orderby,
  				'ignore_sticky_posts'   => $this->ignore_sticky_posts,
  				'paged'                 => $this->paged,
          // 'meta_query' => array(array('key' => '_thumbnail_id'))

  			);

  			// // Post Format & taxonomy !empty($post_format)
  			if(!empty($this->taxonomy)){
  				 $this->args['tax_query'] = array(

                  array(
                       'taxonomy' => $this->taxonomy,
                       'field' => 'slug',
                       'terms' => $this->term_slug,
                  ),
  				 );
  			 }

  			// Post per page
  			if(!empty($this->posts_per_page)){
  				 $this->args['posts_per_page'] = $this->posts_per_page;
  			 }

  			// Post Status
  			if(!empty($this->post_status)){
  				$this->args['post_status'] = $this->post_status;
  			}

  			// Category
  			if(!empty($this->category__in)){
  				$this->args['category__in'] = $this->category__in;
  			}

  			// Category Not In
  			if(!empty($this->category__not_in)){
  				//  $exclude_cats = explode(",",$category__not_in);
  				$this->args['category__not_in'] = $this->exclude_cats;
  			}

  		 // Tag
  			if(!empty($this->tag__in)){
  				//  $include_tags = explode(",",$tag__in);
  				$this->args['tag__in'] = $this->include_tags;
  			}

  			// Tag Not In
  			if(!empty($this->tag__not_in)){
  				//  $exclude_tags = explode(",",$tag__not_in);
  				$this->args['tag__not_in'] = $this->exclude_tags;
  			}
       //
  		// 	// Date (not using date_query as there was issue with year/month archives)
  		// 	if(!empty($year)){
  		// 			$this->$args['year'] = $year;
  		// 	 }
       //
  		// 	//  print_r($month);
       //
  		// 	//  if(!empty($months)){
  		// 	// 		$args['monthnum'] = $month;
  		// 	//    }
       //
  		// 	 if(!empty($day)){
  		// 			$this->$args['day'] = $day;
  		// 	 }
       //
  			// Meta Query
  			// if(!empty($this->meta_key) && !empty($this->meta_value)){
  			// 	$this->args['meta_query'] = array(
  			// 		 ewpq_get_meta_query($meta_key, $meta_value, $meta_compare)
  			// 	);
  			//  }
       //
  			// Author
  			if(!empty($this->author_id)){
  				$this->args['author'] = $this->author_id;
  			}
        // Featured Image
        if( $this->featured_image){
          $this->args['meta_query'] = array(array('key' => '_thumbnail_id'));
        }

  		//  // Search Term
  		// 	if(!empty($s)){
  		// 		$this->$args['s'] = $s;
  		// 	}
       //
  		// 		 // Meta_key, used for ordering by meta value
  		// 		 if(!empty($meta_key)){
  		// 				$this->$args['meta_key'] = $meta_key;
  		// 		 }
       //
  			// Include posts
  			if(!empty($this->post__in)){
  				$this->args['post__in'] = $this->post__in;
  			}
       //
  		// 	// Exclude posts
  		// 	if(!empty($this->post__not_in)){
  		// 		// 		$post__not_in = explode(",",$post__not_in);
  		// 		$this->$this->$args['post__not_in'] = $post__not_in;
  		// 	}
       //
  		// 	// Language
  		// 	if(!empty($lang)){
  		// 		$this->$args['lang'] = $lang;
  		// 	}

  		return $this->query = new WP_Query( $this->args );

  }

  // public function vb_archive(){
  //
  //   // $template = XTW_PLUGIN_PATH .'templates/'.$this->query_template.'/'. $this->query_template.'.php';
  //   //
  //   // // Start the loop.
  //   // while ( have_posts() ) : the_post();
  //   //
  //   //   if (file_exists( $template )){
  //   //     require( XTW_PLUGIN_PATH .'templates/'.$this->query_template.'/'. $this->query_template.'.php' );
  //   //   }
  //   //
  //   // endwhile;
  //
  //
  //   // if (have_posts()) :
  //   //
  //   //     while (have_posts()) : the_post();
  //   //
  //   //     the_title( );
  //   //     the_content();
  //   //
  //   //    endwhile;
  //   //
  //   //
  //   //
  //   //   endif;
  //
  // }

public function vb_display_single_navigation() {

  if ( ! is_single() ) {
    return false;
  }

/**
* *https://codex.wordpress.org/Function_Reference/next_post_link
* @todo in taxonomy
* @todo prev / next thumbnail
*
**/
  // if ( ! $this->get_setting( 'show-single-post-navigation', true ) ) {
  //   return false;
  // }
  //
  // if ( $this->get_setting( 'mode', 'default' ) == 'custom-query' ) {
  //   return false;
  // }
  //
  // if ( $this->get_setting( 'show-single-post-navigation-enable-tax', true ) ) {
  //   if ( ! $this->get_setting( 'show-single-post-navigation-tax' ) ) {
  //     $enable_tax = 'category';
  //   }  // the default
  //   else {
  //     $enable_tax = $this->get_setting( 'show-single-post-navigation-tax' );
  //   }
  // } else {
  //   $enable_tax = '';
  // }

  echo '<div id="nav-below" class="loop-navigation single-post-navigation loop-utility loop-utility-below" itemscope itemtype="http://schema.org/SiteNavigationElement">';

      echo '<div class="nav-previous" itemprop="url">';
      // previous_post_link( '%link', '<span class="meta-nav">&larr;</span> %title', true, ' ', $enable_tax );
      previous_post_link( '%link', '<span class="meta-nav">&larr;</span> %title' );
      echo '</div>';

      echo '<div class="nav-next" itemprop="url">';
      // next_post_link( '%link', '%title <span class="meta-nav">&rarr;</span>', true, ' ', $enable_tax );
        next_post_link( '%link', '%title <span class="meta-nav">&rarr;</span>' );
      echo '</div>';

  echo '</div>';

}

/**
*
*   Archive Pagination
*
**/

public function vb_display_pagination() {

  if ( function_exists('wp_pagenavi') ) {

    wp_pagenavi();

  } else {

    $older_posts_text = __( '<span class="meta-nav">&larr;</span> Older posts', 'headway' );
    $newer_posts_text = __( 'Newer posts <span class="meta-nav">&rarr;</span>', 'headway' );

    echo '<div class="nav-previous" itemprop="url">' . get_next_posts_link( $older_posts_text, $this->query->max_num_pages ) . '</div>';
    echo '<div class="nav-next" itemprop="url">' . get_previous_posts_link( $newer_posts_text ) . '</div>';

  }
}

/**
*
* @todo rename archive + single...
*
**/

  public function vb_single(){

    $template = XTW_PLUGIN_PATH .'templates/'.$this->query_template.'/'. $this->query_template.'.php';

    // Start the loop.
    while ( have_posts() ) : the_post();

      if (file_exists( $template )){
        require( XTW_PLUGIN_PATH .'templates/'.$this->query_template.'/'. $this->query_template.'.php' );
      }

    endwhile;

    $this->vb_display_single_navigation();
    $this->vb_display_pagination();
    // echo  'strankovanie';
  }

/**
*
*   Posts Loop
*
**/

public function vb_loop( $full_width_last = false ) {

if (!is_archive()) {
  self::vb_query();
}


// If no query is specified, grab the main query
// global $wp_query;
// if( !isset( $query ) || empty( $query ) || !is_object( $query ) )
//   $query = $wp_query;


/**
*
*   Temp archive Loop - alternative
*
**/

if (is_archive()) {

  $this->paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

  $this->post_type = get_queried_object();

  $current_post_type = $this->post_type->query_var;

  // $args = array(
  //     'post_type' => $current_post_type,
  //     'paged' => $this->paged,
  //     'posts_per_page' => '4'
  // );
  //
  // $this->query = new WP_Query($args);
  //

}



/**
*
*  END Temp archive Loop - alternative
*
**/



/**
*	Set max_columns for page width
*
* $columns_md moze byt len delitelne $columns_lg
*	$columns_lg = 4; $columns_md = 2
*
**/

if( $this->columns_lg ) {

  $max_columns = $this->columns_lg;

} else {

  $max_columns = $this->columns_md;

}

// $max_columns = 2; //columns will arrange to any number (as long as it is evenly divisible by 12)
$column_md = 12 / $this->columns_md; //column number
$column_lg = 12 / $this->columns_lg; //column number

$total_items = $this->query->post_count;

$remainder = $this->query->post_count % $max_columns; //how many items are in the last row
$first_row_item = ( $total_items - $remainder ); //first item in the last row

/**
*
*   Loop
*
**/

#   File Name path
$template = XTW_PLUGIN_PATH .'templates/'.$this->query_template.'/'. $this->query_template.'.php';

$i = 0; // counter

/**
Archive loop
**/

while(have_posts()) {
   the_post();

  // if( have_posts() ) {
  // while ( $this->query->have_posts() ) {
  //   $this->query->the_post();

  // if counter is multiple of N
  if ( $i % $max_columns == 0 ) { ?>
  <div class="row">
  <?php
  // Open Last Column If $full_width_last
  } if ( $i >= $first_row_item && $full_width_last ) {  ?>
    <div class="col-md-<?php echo 12 / $remainder . " col-lg-". 12 / $remainder ?>">
  <?php
  // Open Column
  } else { ?>
    <div class="col-md-<?php echo $column_md . " col-lg-".$column_lg ?>">
  <?php }

    /**
    *	Post Template Part
    *	@todo includovanie nahradit .get_files, scandir - class-fpw-widget.php
    *
    **/
    if (file_exists( $template )){
      require( XTW_PLUGIN_PATH .'templates/'.$this->query_template.'/'. $this->query_template.'.php' );
    }
    ?>
    <?php edit_post_link(__('Edit', 'xtweaks')); ?>
    </div>
    <?php
    // Close Row - if counter is multiple of 3
      $i++; if( $i % $max_columns == 0 ) {
    ?>
  </div>
<?php }
 } // endwhile ?>
 <?php if( $i % $max_columns != 0 ) {
// Close Row if loop is not exactly a multiple of N
?>
</div>
<?php

  } // end While

// }

/**
* Pagination
**/
$this->vb_display_single_navigation();
$this->vb_display_pagination();

// Quick edit View / Query
echo '<a href='.get_edit_post_link( $this->id_vb_query ).'>Edit Query</a> ';
echo '<a href='.get_edit_post_link( $this->id_vb_view ).'>Edit View</a>';

} //vb_query





} // view_builder

?>
