<?php


function possible_row_open() {

  if ( isset( $this->row_open ) && $this->row_open ) {
    return;
  }

  if ( $this->get_setting( 'mode', 'default' ) == 'default' && is_singular() ) {
    return;
  }

  if ( ! ( is_search() || $this->paged > 1 ) && $this->count <= $this->get_setting( 'featured-posts', 1 ) ) {
    return;
  }

  echo "\n\n" . '<div class="entry-row">' . "\n\n";

  $this->row_open = true;

}


function possible_row_close() {

  $posts_per_row = ( $this->get_setting( 'enable-column-layout' ) && ! ( is_singular() && $this->get_setting( 'mode', 'default' ) == 'default' ) ) ? $this->get_setting( 'posts-per-row', '2' ) : 1;

  /* If a row isn't open then we don't have anything to close */
  if ( ! isset( $this->row_open ) || ! $this->row_open ) {
    return false;
  }

  $featured_posts_on_page = ! ( is_search() || $this->paged > 1 ) ? $this->get_setting( 'featured-posts', 1 ) : 0;

  /* Only run the every nth post check if it's not the last post.  If it is the last post, then we have to close the row no matter what. */
  if ( $this->count < $this->query->post_count ) {

    /* Normal check to close.  Close every nth post */
    if ( ( $this->count - $featured_posts_on_page ) % $posts_per_row !== 0 ) {
      return false;
    }

  }

  echo "\n\n" . '</div>' . "\n\n";

  $this->row_open = false;

}

 ?>
