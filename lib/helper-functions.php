<?php

// // String to Boolean  - > 0 to FALSE, 1 to TRUE
//
// function true_false($var){
//
// 	return	filter_var($var, FILTER_VALIDATE_BOOLEAN); // bool(true)
//
// }


/**
  Remove &lt;some&gt; tags from WP admin columns Title
  * sample -> <span>Some Custom Post Title</span> continue title text
  * to ->  Some Custom Post Title continue title text
**/
add_action( 'admin_head-edit.php', 'wpse152971_edit_post_change_title_in_list' );

function wpse152971_edit_post_change_title_in_list() {
    add_filter( 'the_title','wpse152971_construct_new_title', 100,  2 );
}

function wpse152971_construct_new_title( $title, $id ) {

    $title = preg_replace_callback(
    		'/&lt;([\s\S]*?)&gt;/s',
    		function ($matches) {
    			// do whatever you need with $matches here, e.g. save it somewhere
    			return '';
    		},
    		$title
    	);
    return $title;
}



/**
*
*		Social Sharing
*
**/

function xtw_social_sharing_buttons($content) {
	if(is_singular() || is_home()){

		// Get current page URL
		$crunchifyURL = get_permalink();

		// Get current page title
		$crunchifyTitle = str_replace( ' ', '%20', get_the_title());

		// Get Post Thumbnail for pinterest
		$crunchifyThumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );

		// Construct sharing URL without using any script
		$twitterURL = 'https://twitter.com/intent/tweet?text='.$crunchifyTitle.'&amp;url='.$crunchifyURL;
		$facebookURL = 'https://www.facebook.com/sharer/sharer.php?u='.$crunchifyURL.'&t='.get_the_title();
		$googleURL = 'https://plus.google.com/share?url='.$crunchifyURL;
		$bufferURL = 'https://bufferapp.com/add?url='.$crunchifyURL.'&amp;text='.$crunchifyTitle;

		// Based on popular demand added Pinterest too
		$pinterestURL = 'https://pinterest.com/pin/create/button/?url='.$crunchifyURL.'&amp;media='.$crunchifyThumbnail[0].'&amp;description='.$crunchifyTitle;

		// Add sharing button at the end of page/page content
		$content .= '<div class="social-share-social">';
		$content .= '<h5>ZDIEĽAJTE</h5>';
		$content .=  '<div class="social-share-wrapper">';
    $content .= '<a class="social-share-link social-share-twitter" href="'. $twitterURL .'" target="_blank">Twitter</a>';
		$content .= '<a class="social-share-link social-share-facebook" href="'.$facebookURL.'" target="_blank">Facebook</a>';
		$content .= '<a class="social-share-link social-share-googleplus" href="'.$googleURL.'" target="_blank">Google+</a>';
		$content .= '<a class="social-share-link social-share-buffer" href="'.$bufferURL.'" target="_blank">Buffer</a>';
		$content .= '<a class="social-share-link social-share-pinterest" href="'.$pinterestURL.'" target="_blank">Pin It</a>';
		$content .= '</div>';
		$content .= '</div>';

		return $content;
	} else {
		// if not a post/page then don't include sharing button
		return $content;
	}
}


/**
*
* 	Add css class to edit button
*
**/

function custom_edit_post_link( $output ) {
 $output = str_replace('class="post-edit-link"', 'class="post-edit-link-arc"', $output);
 return $output;
}

/**
*
*		Delete Post link
*
**/

function xtw_edit_delete_link(){

    global $post;
    if ( !current_user_can( 'edit_post', $post->ID ) )
    return;

    if( is_user_logged_in() ){
        add_filter('edit_post_link', 'custom_edit_post_link');
        edit_post_link('upraviť', '', '');
        echo '&nbsp; <a href="'.get_delete_post_link( ).'" class="delete-post">do koša</a>';
    }
}


/**
*
*		SLUG from string
*
**/

function xtw_slugify($text) {
  // replace non letter or digits by -
  $text = preg_replace('~[^\pL\d]+~u', '-', $text);

  // transliterate
  $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

  // remove unwanted characters
  $text = preg_replace('~[^-\w]+~', '', $text);

  // trim
  $text = trim($text, '-');

  // remove duplicate -
  $text = preg_replace('~-+~', '-', $text);

  // lowercase
  $text = strtolower($text);

  if (empty($text))
  {
    return 'n-a';
  }

  return $text;
}


/**
*		Dev functionality
**/

/**
*
*		List Hooked Functions
*
**/

function list_hooked_functions( $tag = false ){
  global $wp_filter;
  if ($tag) {

  $hook[$tag] = $wp_filter[$tag];
  if ( !is_array( $hook[$tag] )) {
    trigger_error("Nothing found for '$tag' hook", E_USER_WARNING);
  return;
  }
 }
 else {
  $hook = $wp_filter;
  ksort($hook);
 }
 echo '<pre>';
 foreach($hook as $tag => $priority){
  echo "<br />&gt;&gt;&gt;&gt;&gt;\t<strong>$tag</strong><br />";
  ksort( $priority );
  foreach( $priority as $priority => $function ){
  echo $priority;
  // foreach($function as $name => $properties) echo "\t$name<br />";
  }
 }
 echo '</pre>';
 return;
}

// list_hooked_functions();

/**
*
*   DEV
*   NOTE: masove zmazanie terms
*
**/

function sjc_delete_terms() {

    # setup
    $term = 'cities';

     if ( is_admin() ) {
          $terms = get_terms( $term, array( 'fields' => 'ids', 'hide_empty' => false ) );
          foreach ( $terms as $value ) {
               wp_delete_term( $value, $term );
          }
     }
}
// add_action( 'init', 'sjc_delete_terms' );


?>
