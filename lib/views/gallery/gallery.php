<?php
$main_wrapper = 'div';

?>
<div class="artwork creative">
    <div class="gallery-item">
        <div class="gallery-thumb">
          <?php if ( has_post_thumbnail() ) {
              the_post_thumbnail('thumbnail');
            } ?>
            <div class="image-overlay"></div>
            <a href="xxx" class="gallery-zoom"><i class="fa fa-eye" alt="This is the title"></i></a>
            <a href="#" class="gallery-link" target="_blank"><i class="fa fa-link"></i></a>
        </div>
        <div class="gallery-details">
          <?php the_title( '<h3>', '</h3>' ); ?>
            <?php the_content(); ?>
        </div>
    </div>
</div>
