<?php
/**
* @TODO
**/ ?>
     <div class="row">
         <div class="span6">
           <?php if ( has_post_thumbnail() ) { ?>
           <a href="#" class="post-thumbnail post-thumbnail-left">

             <?php the_post_thumbnail(); ?>
            </a>
            <?php } ?>
         </div>
         <div class="span6 tabs-content">
         <?php the_title( '<h3>', '</h3>' ); ?>

          <?php
          if (redux_post_meta( "redux_tweaks", $this->vb_view->ID, "vb-excerpt-content") ){
            // echo mb_substr(get_the_content(), 0, redux_post_meta( "redux_tweaks", $this->vb_view->ID, "vb-excerpt-shorten") );
            echo mb_substr(wp_strip_all_tags(get_the_content()), 0, redux_post_meta( "redux_tweaks", $this->vb_view->ID, "vb-excerpt-shorten"));
          } else the_content(); ?>
          <?php //xtw_edit_delete_link(); ?>
         </div>
     </div>
