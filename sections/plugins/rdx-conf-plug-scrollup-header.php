<?php 

/**
*
* ScrollUp Header
*
**/

Redux::setSection( $opt_name, // This is your opt_name redux_tweaks
    array(
        'icon'       => 'el-icon-minus',
        'title'      => __( 'ScrollUp Header', 'redux-framework-demo' ),
        'subsection' => true,
        'fields'     => array(

            array(
                'id'       => 'opt-scrollup-header',
                'type'     => 'switch', 
                'title'    => __('ScrollUp Header', 'redux-framework-demo'),
                'subtitle' => __('* Potrebne main.less', 'redux-framework-demo'),
                'default'  => '0',
            ),

            array(
                'id'       => 'opt-scrollup-header-id-class',
                'type'     => 'text', 
                'title'    => __('Wrapper identificator', 'redux-framework-demo'),
                'subtitle' => __('Class or ID', 'redux-framework-demo'),
               // 'default'  => true,
            ),

             array(
                'id'   => 'info-scrollup-header',
                'type' => 'info',
                'title'    => __('Scroll Up', 'redux-framework-demo'),
                //'desc'     => __('Desc', 'redux-framework-demo'),
                'notice' => true,
                //'icon'  => 'el-icon-info-sign',
                'style' => 'success', // warning|critical|info|success
            ),

            array(
                'id'       => 'info-opt-scrollup-header',
                'type'     => 'info', 
                'title'    => __('stickUp2 - Git', 'redux-framework-demo'),
                'subtitle' => __('<a href="https://github.com/ppowalowski/stickUp2">Github stickUp2 </a>', 'redux-framework-demo'),
               // 'default'  => true,
            ),
            
        )
    )
);
 ?>