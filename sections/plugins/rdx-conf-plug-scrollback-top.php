<?php 

/**

Scroll Back to top

**/

Redux::setSection( $opt_name, // This is your opt_name redux_tweaks
    array(
    'icon'       => 'el-icon-chevron-up',
    'title'      => __( 'Scroll Back to Top', 'redux-framework-demo' ),
    'subsection' => true,
    'fields'     => array(

        array(
            'id'       => 'opt-scroll-back',
            'type'     => 'switch', 
            'title'    => __('Scroll Back to Top', 'redux-framework-demo'),
            'subtitle' => __('', 'redux-framework-demo'),
            'default'  => '0',
        ),

/**
Visibility Status
**/
        // array(
        //     'id'       => 'opt-status-scroll-back',
        //     'type'     => 'select',
        //     'title'    => __( 'Status', 'redux-framework-demo' ),
        //     'subtitle' => __( 'No validation can be done on this field type', 'redux-framework-demo' ),
        //     'desc'     => __( 'This is the description field, again good for additional info.', 'redux-framework-demo' ),
        //     //Must provide key => value pairs for radio options
        //     'options'  => array(
        //         'publicly_visible' => 'Publicly Visible',
        //         'preview_mode' => 'Preview mode',
        //         'disabled' => 'Disabled'
        //     ),
        //     'default'  => 'disabled'
        // ),

/**
Dimension
**/
        array(
            'id'       => 'opt-scroll-back-dimension',
            'required' => array( 'opt-scroll-back', '=', true ),
            'type'     => 'dimensions',
            'units'    => array('em','px','%'),
            'title'    => __('Dimensions W/H', 'redux-framework-demo'),
            // 'subtitle' => __('Allow your users to choose width, height, and/or unit.', 'redux-framework-demo'),
            // 'desc'     => __('Enable or disable any piece of this field. Width, Height, or Units.', 'redux-framework-demo'),
            'default'  => array(
                'width'   => '30', 
                'height'  => '30'
            ),
        ),

/**
Icon
**/

        array(
            'id'   => 'info-scroll-back-icon',
            'required' => array( 'opt-scroll-back', '=', true ),
            'type' => 'info',
            'title' => __('Icon / Text', 'redux-framework-demo'),
            // 'desc' => __('desc', 'redux-framework-demo'),
            'notice' => true,
            //'icon'  => 'el-icon-info-sign',
            'style' => 'success', // warning|critical|info
        ),
        array(
            'id'            => 'opt-scroll-back-icon-size',
            'required' => array( 'opt-scroll-back', '=', true ),
            'type'          => 'spinner',
            'title'         => __( 'Icon size', 'redux-framework-demo' ),
            'default'       => 4,
            'min'           => 0,
            'step'          => 1,
            'max'           => 40,
            'display_value' => 'text'
        ),

        array(
            'id'       => 'opt-scroll-back-icon',
            'required' => array( 'opt-scroll-back', '=', true ),
           'type'     => 'select',
            'select2'  => array( 'containerCssClass' => '' ),
            'title'    => 'Arrow Icon',
            'subtitle' => '',
            'desc'      => 'Pozor moze kolidovat s inymi pluginmi s awesome icons',
            'class'    => ' font-icons',
            'options'  => $iconArray,
            'default'   => 'chevron-up',
        ),


        array(
            'id'       => 'opt-scroll-back-label',
            'required' => array( 'opt-scroll-back', '=', true ),            
            'type'     => 'text',
            'title'    => __( 'Custom Label text', 'redux-framework-demo' ),
        ),
/**
Color
**/
        
        array(
            'id'   => 'info-scroll-back-background',
            'required' => array( 'opt-scroll-back', '=', true ),
            'type' => 'info',
            'title' => __('Background', 'redux-framework-demo'),
            // 'desc' => __('desc', 'redux-framework-demo'),
            'notice' => true,
            //'icon'  => 'el-icon-info-sign',
            'style' => 'success', // warning|critical|info
        ),

        array(
            'id'       => 'opt-scroll-back-background',
            'required' => array( 'opt-scroll-back', '=', true ),
            'type'     => 'color_rgba',
            'title'    => __( 'Background', 'redux-framework-demo' ),
            // 'subtitle' => __( 'Gives you the RGBA color. Still quite experimental. Use at your own risk.', 'redux-framework-demo' ),
            'default'  => array( 'color' => '#555', 'alpha' => '1.0' ),
            'clickout_fires_change'     => true,
            'validate' => 'colorrgba',
        ),

        array(
            'id'       => 'opt-scroll-back-hover-background',
            'required' => array( 'opt-scroll-back', '=', true ),
            'type'     => 'color_rgba',
            'title'    => __( 'Hover Background', 'redux-framework-demo' ),
            // 'subtitle' => __( 'Gives you the RGBA color. Still quite experimental. Use at your own risk.', 'redux-framework-demo' ),
            'default'  => array( 'color' => '#555', 'alpha' => '1.0' ),
            'clickout_fires_change'     => true,
            'validate' => 'colorrgba',
        ),
        


        array(
            'id'   => 'info-scroll-back-color',
            'required' => array( 'opt-scroll-back', '=', true ),
            'type' => 'info',
            'title' => __('Color', 'redux-framework-demo'),
            // 'desc' => __('desc', 'redux-framework-demo'),
            'notice' => true,
            //'icon'  => 'el-icon-info-sign',
            'style' => 'success', // warning|critical|info
        ),
        array(
            'id'       => 'opt-scroll-back-hover',
            'required' => array( 'opt-scroll-back', '=', true ),
            'type'     => 'color_rgba',
            'title'    => __( 'Hover', 'redux-framework-demo' ),
            // 'subtitle' => __( 'Gives you the RGBA color. Still quite experimental. Use at your own risk.', 'redux-framework-demo' ),
            'default'  => array( 'color' => '#000', 'alpha' => '1.0' ),
            'validate' => 'colorrgba',
        ),

        array(
            'id'       => 'opt-scroll-back-color',
            'required' => array( 'opt-scroll-back', '=', true ),
            'type'     => 'color_rgba',
            'title'    => __( 'Color', 'redux-framework-demo' ),
            'default'  => array( 'color' => '#fff', 'alpha' => '1.0' ),
            'validate' => 'colorrgba',
        ),


/**
Border Radius
**/

        array(
            'id'   => 'info-scroll-back-border',
            'required' => array( 'opt-scroll-back', '=', true ),
            'type' => 'info',
            'title' => __('Border', 'redux-framework-demo'),
            // 'desc' => __('desc', 'redux-framework-demo'),
            'notice' => true,
            //'icon'  => 'el-icon-info-sign',
            'style' => 'success', // warning|critical|info
        ),

        array(
            'id'            => 'opt-scroll-back-border-radius',
            'required' => array( 'opt-scroll-back', '=', true ),
            'type'          => 'spinner',
            'title'         => __( 'Border radius', 'redux-framework-demo' ),
            'default'       => 3,
            'min'           => 0,
            'step'          => 1,
            'max'           => 500,
            'display_value' => 'text'
        ),

/**
Border Style
**/
        array(
            'id'       => 'opt-scroll-back-border',
            'required' => array( 'opt-scroll-back', '=', true ),
            'type'     => 'border',
            'title'    => __('Border', 'redux-framework-demo'),
            'output'   => array('.site-header'),
            'all' =>    true,
            'default'  => array(
                'border-color'  => '#555', 
                'border-style'  => 'none', 
                'border-width'  => '1px',
                'border-top'    => '1px', 
                'border-right'  => '1px', 
                'border-bottom' => '1px', 
                'border-left'   => '1px'
            ),  
        ),
        // array(
        //     'id'            => 'opt-slider-text',
        //     'type'          => 'slider',
        //     'title'         => __( 'Slider Example 2 with Steps (5)', 'redux-framework-demo' ),
        //     'subtitle'      => __( 'This example displays the value in a text box', 'redux-framework-demo' ),
        //     'desc'          => __( 'Slider description. Min: 0, max: 300, step: 5, default value: 75', 'redux-framework-demo' ),
        //     'default'       => 75,
        //     'min'           => 0,
        //     'step'          => 5,
        //     'max'           => 300,
        //     'display_value' => 'text'
        // ),


/**
Alignment
**/

        array(
            'id'   => 'info-scroll-back-alignment',
            'required' => array( 'opt-scroll-back', '=', true ),
            'type' => 'info',
            'title' => __('Alignment', 'redux-framework-demo'),
            // 'desc' => __('desc', 'redux-framework-demo'),
            'notice' => true,
            //'icon'  => 'el-icon-info-sign',
            'style' => 'success', // warning|critical|info
        ),
        array(
            'id'       => 'opt-scroll-back-vertical-alignment',
            'required' => array( 'opt-scroll-back', '=', true ),
            'type'     => 'select',
            'title'    => __( 'Vetical Alignment', 'redux-framework-demo' ),
            //Must provide key => value pairs for radio options
            'options'  => array(
                'left' => 'Left',
                'center' => 'Center',
                'right' => 'Right'
            ),
            'default'  => 'right'
        ),

        array(
            'id'       => 'opt-scroll-back-horizontal-alignment',
            'required' => array( 'opt-scroll-back', '=', true ),
            'type'     => 'select',
            'title'    => __( 'Horizontal Alignment', 'redux-framework-demo' ),
            //Must provide key => value pairs for radio options
            'options'  => array(
                'top' => 'Top',
                'bottom' => 'Bottom',
            ),
            'default'  => 'bottom'
        ),

/**
Distance
**/

        
        array(
            'id'   => 'info-scroll-back-distance',
            'required' => array( 'opt-scroll-back', '=', true ),
            'type' => 'info',
            'title' => __('Distance', 'redux-framework-demo'),
            // 'desc' => __('desc', 'redux-framework-demo'),
            'notice' => true,
            //'icon'  => 'el-icon-info-sign',
            'style' => 'success', // warning|critical|info
        ),

        array(
            'id'            => 'opt-scroll-back-horizontal-distance',
            'required' => array( 'opt-scroll-back', '=', true ),
            'type'          => 'spinner',
            'title'         => __( 'Horizontal Distance', 'redux-framework-demo' ),
            'default'       => 30,
            'min'           => 0,
            'step'          => 1,
            'max'           => 300,
            'display_value' => 'text'
        ),

        array(
            'id'            => 'opt-scroll-back-vertical-distance',
            'required' => array( 'opt-scroll-back', '=', true ),
            'type'          => 'spinner',
            'title'         => __( 'Vertical Distance', 'redux-framework-demo' ),
            'default'       => 30,
            'min'           => 0,
            'step'          => 1,
            'max'           => 300,
            'display_value' => 'text'
        ),

/**
Text
**/

        array(
            'id'   => 'info-scroll-back-font',
            'required' => array( 'opt-scroll-back', '=', true ),
            'type' => 'info',
            'title' => __('Font', 'redux-framework-demo'),
            // 'desc' => __('desc', 'redux-framework-demo'),
            'notice' => true,
            //'icon'  => 'el-icon-info-sign',
            'style' => 'success', // warning|critical|info
        ),

        array(
            'id'            => 'opt-scroll-back-font-size',
            'required' => array( 'opt-scroll-back', '=', true ),
            'type'          => 'spinner',
            'title'         => __( 'Font size', 'redux-framework-demo' ),
            'default'       => 14,
            'min'           => 0,
            'step'          => 2,
            'max'           => 300,
            'display_value' => 'text'
        ),

        array(
            'id'            => 'opt-scroll-back-line-height',
            'required' => array( 'opt-scroll-back', '=', true ),
            'type'          => 'spinner',
            'title'         => __( 'Line Height', 'redux-framework-demo' ),
            'default'       => 14,
            'min'           => 0,
            'step'          => 2,
            'max'           => 300,
            'display_value' => 'text'
        ),

/**
Animation
**/

        array(
            'id'   => 'info-scroll-back-animation',
            'required' => array( 'opt-scroll-back', '=', true ),
            'type' => 'info',
            'title' => __('Animation', 'redux-framework-demo'),
            // 'desc' => __('desc', 'redux-framework-demo'),
            'notice' => true,
            //'icon'  => 'el-icon-info-sign',
            'style' => 'success', // warning|critical|info
        ),
        
        array(
            'id'            => 'opt-scroll-back-scroll-duration',
            'required' => array( 'opt-scroll-back', '=', true ),
            'type'          => 'slider',
            'title'         => __( 'Scroll Duration', 'redux-framework-demo' ),
            'subtitle'      => __( 'This example displays the value in a text box', 'redux-framework-demo' ),
            'desc'          => __( 'Slider description.', 'redux-framework-demo' ),
            'default'       => 500,
            'min'           => 0,
            'step'          => 50,
            'max'           => 3000,
            'display_value' => 'text'
        ),

        array(
            'id'            => 'opt-scroll-back-fade-duration',
            'required' => array( 'opt-scroll-back', '=', true ),
            'type'          => 'slider',
            'title'         => __( 'Fade Duration', 'redux-framework-demo' ),
            'subtitle'      => __( 'This example displays the value in a text box', 'redux-framework-demo' ),
            'desc'          => __( 'Slider description.', 'redux-framework-demo' ),
            'default'       => 500,
            'min'           => 0,
            'step'          => 50,
            'max'           => 3000,
            'display_value' => 'text'
        ),
    )
    )
);
?>