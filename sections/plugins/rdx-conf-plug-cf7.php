<?php

Redux::setSection( $opt_name, // This is your opt_name redux_tweaks

//http://safo/wp-admin/admin.php?page=wpcf7
 array(
        'icon'       => 'el-icon-broom',
        'title'      => __( 'CF7 to CPT', 'redux-framework-demo' ),
        'subsection' => true,
        'fields'     => array(
/**

CF 7 to CPT - Contact form to Custom Post Type

**/
        array(
            'id'       => 'opt-select-post-type-cf7',
            'type'     => 'select',
            'data'     => 'post_type',
            'title'    => __( 'Post type', 'redux-framework-demo' ),
            // 'subtitle' => __( 'No validation can be done on this field type', 'redux-framework-demo' ),
            'desc'     => __( 'Select post type', 'redux-framework-demo' ),
        ),
/**
Post status
**/
        array(
            'id'       => 'opt-select-post-status-cf7',
            'type'     => 'select',
            'title'    => __( 'Post status', 'redux-framework-demo' ),
            // 'subtitle' => __( 'No validation can be done on this field type', 'redux-framework-demo' ),
            'desc'     => __( 'Default Post status', 'redux-framework-demo' ),
            //Must provide key => value pairs for select options
            'options'  => array(
                'publish' => 'publish', // - A published post or page
                'pending' => 'pending', // - post is pending review
                'draft' => 'draft', // - a post in draft status
                'auto-draft' => 'auto-draft', //  - a newly created post, with no content
                'future' => 'future', // - a post to publish in the future
                'private' =>'private', // - not visible to users who are not logged in
                'inherit' => 'inherit', //- a revision. see get_children.
                'trash' => 'trash', // - post is in trashbin. added with Version 2.9.*/
            ),
            'default'  => 'pending'
        ),

/**
Form Id
**/
        array(
            'id'            => 'opt-text-form-id-cf7',
          //  'required'      => array( 'opt-id', '=', true ),
            'type'          => 'text',
            'title'         => __('Form ID', 'redux-framework-demo'),
            //'subtitle'    => __('Subtitle', 'redux-framework-demo'),
            //'desc'        => __('Desc', 'redux-framework-demo'),
            //'default'       => 'default',
            'ajax_save'     => true
        ),


        array(
            'id'   => 'raw-cf7',
            // 'required' => array( 'opt-id', '=', true ),
            'type' => 'raw',
            'title'    => __('Sample', 'redux-framework-demo'),
            //'subtitle' => __('Subtitle', 'redux-framework-demo'),
            'desc'     => __('<a href="/wp-admin/admin.php?page=wpcf7">Setup CF7</a>', 'redux-framework-demo'),
            'full_width’ => false',
            // 'notice' => true,
            'content'  => '[honeypot your-surname][text* your-name placeholder "Vaše meno"]<br />
                            [email* your-email placeholder "Váš e-mail"]<br />
                            [textarea* your-message placeholder "Váš dotaz"]<br />
                            [submit "Odoslať dotaz"]<br />'
        ),

    ),
    )
);


?>
