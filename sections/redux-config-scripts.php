<?php
// tweaks/frontend/frontend-scripts.php
Redux::setSection( $opt_name, // This is your opt_name redux_tweaks
    array(
        'icon'       => 'el-icon-braille',
        'title'      => __( 'Components', 'redux-framework-demo' ),
        'submenu' => true,
        'subsection' => false,
        'fields'     => array(
        /*has_archive
        "labels" => $labels,
                "description" => "",
                "public" => true,
                "show_ui" => true,
                "has_archive" => false,
                "show_in_menu" => true,
                'menu_position' => 10,
                "exclude_from_search" => false,
                "capability_type" => "post",
                "map_meta_cap" => true,
                "hierarchical" => false,
                "rewrite" => array( "slug" => "recipes", "with_front" => true ),
           */
        )
    )
);
Redux::setSection( $opt_name, // This is your opt_name redux_tweaks

 array(
        'icon'       => 'el-icon-broom',
        'title'      => __( 'Components', 'redux-framework-demo' ),
        'subsection' => true,
        'fields'     => array(

/**

Slick Slider

**/
        array(
            'id'       => 'opt-slick-slider-js',
            'type'     => 'switch',
            'title'    => __( 'Slick Slider', 'redux-framework-demo' ),
            // 'subtitle' => __( 'Slick Slider', 'redux-framework-demo' ),
            'desc'     => __( '<a href="http://kenwheeler.github.io/slick/" target="_blank">Slick Slider</a>', 'redux-framework-demo' ),
        ),


/**

Responsive Tabs

**/
        array(
            'id'       => 'opt-responsive-tabs-components',
            'type'     => 'switch',
            'title'    => __( 'Responsive Tabs', 'redux-framework-demo' ),
            // 'subtitle' => __( 'Slick Slider', 'redux-framework-demo' ),
            'desc'     => __( '<a href="http://jellekralt.github.io/Responsive-Tabs/" target="_blank">github.io</a> <a href="https://github.com/jellekralt/Responsive-Tabs" target="_blank">github</a>', 'redux-framework-demo' ),
        ),

/**

Typed
https://github.com/benrlodge/typewriter
  https://github.com/mattboldt/typed.js/
**/
        array(
            'id'       => 'opt-typer-animation-text',
            'type'     => 'switch',
            'title'    => __( 'Typer', 'redux-framework-demo' ),
            // 'subtitle' => __( 'Slick Slider', 'redux-framework-demo' ),
            'desc'     => __( '<a href="https://github.com/jasondavis/jquery.typer.js" target="_blank">github</a> <a href="http://codepen.io/jasondavis/full/fHdAt" target="_blank">Demo</a>', 'redux-framework-demo' ),

        ),

        /**

Typed
  https://github.com/mattboldt/typed.js/
**/
        array(
            'id'       => 'opt-typed-animation-text',
            'type'     => 'switch',
            'title'    => __( 'Typed', 'redux-framework-demo' ),
            // 'subtitle' => __( 'Slick Slider', 'redux-framework-demo' ),
            'desc'     => __( '<a href="https://github.com/mattboldt/typed.js/" target="_blank">github</a>', 'redux-framework-demo' ),

        ),
/**
Typed
  https://github.com/mattboldt/typed.js/
**/
        array(
            'id'       => 'opt-filterizr-grid',
            'type'     => 'switch',
            'title'    => __( 'Filterizr', 'redux-framework-demo' ),
            // 'subtitle' => __( 'Slick Slider', 'redux-framework-demo' ),
            'desc'     => __( '<a href="http://yiotis.net/filterizr/" target="_blank">demo</a> <a href="https://github.com/giotiskl/Filterizr" target="_blank">Github</a>', 'redux-framework-demo' ),

        ),

    ),
    )
);

?>
