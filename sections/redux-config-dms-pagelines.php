<?php

Redux::setSection( $opt_name, // This is your opt_name redux_tweaks
    array(
        'icon'       => 'el-icon-adjust',
        'title'      => __( 'DMS', 'redux-framework-demo' ),
        // 'subsection' => true,
        'fields'     => array(
        // array(
        //     'id'       => 'opt-select-stylesheet',
        //     'type'     => 'select',
        //     'title'    => __( 'Theme Stylesheet', 'redux-framework-demo' ),
        //     'subtitle' => __( 'Select your themes alternative color scheme.', 'redux-framework-demo' ),
        //     'options'  => array( 'default.css' => 'default.css', 'color1.css' => 'color1.css' ),
        //     'default'  => 'default.css',
        // ),

        array(
            'id'       => 'opt-disable-dms-editor',
            // 'required' => array( 'opt-id', '=', true ),
            'type'     => 'switch',
            'title'    => __('DMS', 'redux-framework-demo'),
            //'subtitle' => __('Subtitle', 'redux-framework-demo'),
            //'desc'     => __('Desc', 'redux-framework-demo'),
            'default'  => true,
            'ajax_save' => true,
        ),
        //http://docs.reduxframework.com/core/fields/switch/


        )
    )
);

?>
