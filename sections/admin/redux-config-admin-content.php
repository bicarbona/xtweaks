<?php 
/**
 Content
 */
 
Redux::setSection( $opt_name, // This is your opt_name redux_tweaks
    array(
        'icon'       => 'el-icon-website',
        'title'      => __( 'Content', 'redux-framework-demo' ),
        'submenu' => false,
        'subsection' => true,
        'ajax_save' => true,
        'fields'     => array(
    /**
     Headway
     */

            array(
                 'id'       => 'remove-hw-content-styling',
                 'type'     => 'switch', 
                 'title'    => 'Remove HW Content Styling',
                 'desc' => 'headway/library/media/css/content-styling.css',
                 'default'  => false,
                 'ajax_save' => true,
            ),

            array(
                'id'       => 'opt-content-edit-button',
                'type'     => 'switch', 
                'title'    => 'Edit Post Button',
                // 'subtitle' => 'subtitle',
                'default'  => true,
                'ajax_save' => true,
            ),

            array(
                'id'       => 'opt-post-revisions-limit',
                'type'     => 'spinner', 
                'title'    => 'Post Revision Limit',
                'subtitle' => 'subtitle',
                // 'desc'     => 'desc',
                'ajax_save' => true,
                'default'  => '3',
                'min'      => '0',
                'step'     => '1',
                'max'      => '10',
            ),

             array(
                'id'   => 'info-head-meta-info',
                'type' => 'info',
                'title'    => __('Head Meta Info', 'redux-framework-demo'),
                //'subtitle' => __('Subtitle', 'redux-framework-demo'),
                //'desc'     => __('Desc', 'redux-framework-demo'),
                'notice' => true,
                //'icon'  => 'el-icon-info-sign',
                'style' => 'success', // warning|critical|info|success
            ),

            array(
                'id'       => 'opt-remove-header-info',
                'type'     => 'switch', 
                'title'    => 'Head Meta info',
                // 'subtitle' => 'Remove version string from header',
                'desc' => 'Cleanup WLW Manifest Links, RSD Links, Shortlink for posts, WP Generator',
                'default'  => true,
                'ajax_save' => true,
            ),

            array(
                'id'       => 'opt-disable-self-ping',
                'type'     => 'switch', 
                'title'    => 'Disable Self Ping',
                'subtitle' => 'Remove pings to your own blog',
                'desc' => 'Remove pings to your own blog',
                'default'  => true,
                'ajax_save' => true,
            )
        )
    )
);
?>