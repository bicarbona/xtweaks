<?php

// function xtw_get_post_status() {
//   return array('sulin'=>'sulinka', 'kokos' => 'kokoska');
// }

// function stf_get_files( $directory, $filter = array( "*" ) ){
//
//   //	function stf_get_files( $directory, $filter = array( "*" ) ){
//       $results = array(); // Result array
//       $filter = (array) $filter; // Cast to array if string given
//
//       $directory = XTW_PLUGIN_PATH . 'views';
//       // $directory ='/Users/gentleman/Dropbox/Server/wp-projects/option/wp-content/plugins/xtweaks/sections/elements';
//
//       $handler = opendir( $directory );
//       while ( $file = readdir($handler) ) {
//         if( is_dir( $file ) )
//           continue;
//
//         $extension = end( explode( ".", $file ) ); // Eg. "*.jpg"
//
//         if ( $file != "." && $file != ".." && ( in_array( $extension, $filter ) || in_array( "*", $filter ) ) ) {
//
//           $results[ $file ] =$file;
//         }
//       }
//       closedir($handler);
//       return $results;
// }

function stf_get_files( $directory, $filter = array( "*" ) ){

  global $custom_theme_plugin_text_domain;

      $results = array(); // Result array
      $filter = (array) $filter; // Cast to array if string given

      $directorys = array( XTW_PLUGIN_PATH . 'lib/views', ABSPATH . 'wp-content/plugins/'.$custom_theme_plugin_text_domain . '/views/' );

      //echo $directory = plugin_dir_path(__FILE__) . 'views';

      foreach($directorys as $directory){

        $handler = opendir( $directory );
        while ( $file = readdir($handler) ) {
          if( is_dir( $file ) )
            continue;
          $extension = end( explode( ".", $file ) ); // Eg. "*.jpg"

            if ( $file != "." && $file != ".." && $file !='.DS_Store' && ( in_array( $extension, $filter ) || in_array( "*", $filter ) ) ) {

              $results[ $file ] = $file;
            }
        }

      }
      closedir($handler);
      return $results;
}

/**
 * @param bool $inc_all
 * @param int  $min_level
 *
 * @return array
 */
function xtw_get_authors( $inc_all = true, $min_level = 1 ) {
  // user_level 1 = contributor
// Get authors
  $userslist = get_users();
  $authors   = array();
  if ( $inc_all ) {
    $authors[ 0 ] = 'All';
  }
  foreach ( $userslist as $author ) {
    if ( get_the_author_meta( 'user_level', $author->ID ) >= $min_level ) {
      $authors[ $author->ID ] = $author->display_name;
    }
  }

  return $authors;
}

function xtw_get_custom_post_types() {
  $pzarc_cpts = ( get_post_types( array( '_builtin' => false, 'public' => true ), 'objects' ) );
  $return     = array();
  foreach ( $pzarc_cpts as $key => $value ) {
    $return[ $key ] = $value->labels->name;
  }

  return $return;
}


// print_r(stf_get_files());

if (!function_exists("redux_add_metaboxes_45")):
  function redux_add_metaboxes_45($metaboxes)
  {


// prefix vb
    $viewSection = array();

    $viewSection[ ] = array(
            'title'  => __('Query', 'xtweaks'),
            // 'desc'   => __('Redux Framework', 'xtweaks'),
            'icon'   => 'el-icon-home',
            'fields' => array(
              array(
                      'id'      => 'vb-query-view',
                      'title'   => __('Query', 'fusion-framework'),
                      'desc'    => '',
                      'type'    => 'select',
                      'data'    => 'post',
                      'args' => array('post_type' => array('vb-query')),
                      'default' => 'None',
              ),

              array(
                  'id'       => 'vb-excerpt-content',
                  'type'     => 'switch',
                  'title'    => __('Excerpt / Content', 'redux-framework-demo'),
                  //'subtitle' => __('Subtitle', 'redux-framework-demo'),
                  //'desc'     => __('Desc', 'redux-framework-demo'),
                  'default'  => true,
                  'ajax_save' => true,
              ),

              array(
                  'id'       => 'vb-excerpt-shorten',
                  'required' => array( 'vb-excerpt-content', '=', true ),
                  'type'     => 'spinner',
                  'title'    => __('Excerpt Shorten', 'xtweaks'),
                  //'subtitle' => __('Subtitle', 'redux-framework-demo'),
                  //'desc'     => __('Desc', 'redux-framework-demo'),
                  'ajax_save' => true,
                  'default'  => '50',
                  'min'      => '10',
                  'step'     => '20',
                  'max'      => '800',
              ),

              array(
                      'title'   => __( 'Template', 'xtweaks' ),
                      'id'      => 'vb-template',
                      'desc'    =>  'cesta k templatom ' . XTW_PLUGIN_PATH. 'templates',
                      'type'    => 'select',
                      'data'    => 'callback',
                      'args'    => array( 'stf_get_files' ),
                      // 'default' => 'all',
              ),

              array(
                      'id'       => 'opt-select-image',
                      'type'     => 'select_image',
                      'title'    => __('Select Image', 'xtweaks'),
                      'subtitle' => __('A preview of the selected image will appear underneath the select box.', 'xtweaks'),
                      'desc'     => __('This is the description field, again good for additional info.', 'xtweaks'),
                      'options'  => Array(
                                    Array (
                                         'alt'  => 'Image Name 1',
                                         'img'  => 'http://option/wp-content/plugins/xtweaks/templates/gallery/gallery.png',
                                    ),
                                    Array (
                                         'alt'  => 'Image Name 2',
                                         'img'  => 'http://option/wp-content/plugins/xtweaks/templates/gallery/gallery2.png',
                                    )
                      ),
                      'default'  => 'Image Name 1',
              ),

              // array(
              //     'id'       => 'opt-gallery',
              //     'type'     => 'gallery',
              //     'title'    => __('Add/Edit Gallery', 'xtweaks'),
              //     'subtitle' => __('Create a new Gallery by selecting existing or uploading new images using the WordPress native uploader', 'xtweaks'),
              //     'desc'     => __('This is the description field, again good for additional info.', 'xtweaks'),
              // ),

              array(
                  'id'       => 'vb-featured-image',
                  // 'required' => array( 'opt-id', '=', true ),
                  'type'     => 'switch',
                  'title'    => __('Featured Image', 'xtweaks'),
                  //'subtitle' => __('Subtitle', 'xtweaks'),
                  //'desc'     => __('Desc', 'xtweaks'),
                  // 'default'  => true,
                  // 'ajax_save' => true,
              ),


              array(
                  'id'       => 'vb-columns-lg',
                  // 'required' => array( 'opt-id', '=', true ),
                  'type'     => 'spinner',
                  'title'    => __('Columns Large', 'xtweaks'),
                  'ajax_save' => true,
                  'default'  => '2',
                  'min'      => '1',
                  'step'     => '1',
                  'max'      => '12',
              ),

              array(
                  'id'       => 'vb-columns-md',
                  // 'required' => array( 'opt-id', '=', true ),
                  'type'     => 'spinner',
                  'title'    => __('Columns Middle', 'xtweaks'),
                  'ajax_save' => true,
                  'default'  => '2',
                  'min'      => '1',
                  'step'     => '1',
                  'max'      => '12',
              ),

              array(
                  'id'       => 'vb-columns-sx',
                  // 'required' => array( 'opt-id', '=', true ),
                  'type'     => 'spinner',
                  'title'    => __('Columns Small', 'xtweaks'),
                  'ajax_save' => true,
                  'default'  => '2',
                  'min'      => '1',
                  'step'     => '1',
                  'max'      => '12',
              ),

            ),
    );


/**
Metaboxes for CPT View
**/
    $metaboxes[ ] = array(
            'id'         => 'query-generator-view',
            'title' => __('View', 'xtweaks'),
            'post_types' => array('vb-view'),
            'position'   => 'normal', // normal, advanced, side
            'priority'   => 'high', // high, core, default, low
            'sections'   => $viewSection
    );

/**
Fields for CPT View
**/
    $page_options    = array();
    $page_options[ ] = array(
      //'title'         => __('General Settings', 'xtweaks'),
      'icon_class' => 'icon-large',
      'icon'       => 'el-icon-home',
      'fields'     => array(

        array(
          'id'       => 'vb-pages',
          'type'     => 'select',
          'multi'    => true,
          'title'    => __('Pages', 'xtweaks'),
          // 'ajax_save' => true,
          'select2' => array( 'allowClear' => true ),
          'data'     => 'pages',
          'sortable' => true,
          'default' => ''
        ),

        array(
            'id'       => 'vb-post-status',
            'type'     => 'select',
            'multi'    => true,
            'title'    => __('Post status', 'xtweaks'),
            'data'     => 'callback',
            'select2' => array( 'allowClear' => true ),
            'args'     => array( 'get_post_statuses' ),
            'ajax_save' => true,
        ),

        array(
          'id'       => 'vb-post-type',
          'type'     => 'select',
          'multi'    => true,
          'title'    => __('CPT Include', 'xtweaks'),
          'select2' => array( 'allowClear' => true ),

          'args'    => array( 'xtw_get_custom_post_types' ),
          'data'    => 'callback',
          // 'ajax_save' => true,
          // 'data' => 'post_types'
        ),

        array(
          'title'    => __( 'Sort by', 'xtweaks' ),
          'id'       => 'vb-orderby',
          'type'     => 'button_set',
          'default'  => 'date',
          'cols'     => 6,
          'options'  => array(
            'date'       => 'Date',
            'title'      => 'Title',
            'menu_order' => 'Page order (pages only)',
            'post__in'   => 'Specified',
            'rand'       => 'Random',
            'none'       => 'None',
            'custom'     => 'Custom field'
          ),
          'desc'=>__('Specified only works when the if the source is using the specific posts/page/etc option.','xtweaks'),
          'subtitle' => 'Some hosts disable random as it slows things down significantly on large sites. WPEngine does this. Look in WPE Dashboard, Advanced Configuration.'
        ),

        array(
          'title'   => __( 'Sort direction', 'xtweaks' ),
          'id'      => 'vb-order',
          'type'    => 'button_set',
          'default' => 'DESC',
          'options' => array(
            'ASC'  => 'Ascending',
            'DESC' => 'Descending',
          ),
        ),

        array(
          'title'   => __( 'Post per page', 'xtweaks' ),
          'id'      => 'vb-posts-per-page',
          'type'    => 'spinner',
          'min'     => 0,
          'max'     => 9999,
          'step'    => 1,
          'default' => 0,
          // 'desc'    => '<strong style="color:tomato;">' . __( 'Note: Skipping breaks pagination. This is a known WordPress issue. Also, skipping does not work if no post limit set. Again a WP limitation. Use a high number of posts to show as a workaround', 'xtweaks' ) . '</strong>',
        ),

        array(
          'title'   => __( 'Skip N posts', 'xtweaks' ),
          'id'      => 'vb-skip',
          'type'    => 'spinner',
          'min'     => 0,
          'max'     => 9999,
          'step'    => 1,
          'default' => 0,
          'desc'    => '<strong style="color:tomato;">' . __( 'Note: Skipping breaks pagination. This is a known WordPress issue. Also, skipping does not work if no post limit set. Again a WP limitation. Use a high number of posts to show as a workaround', 'xtweaks' ) . '</strong>',
        ),

        array(
          'title'   => __( 'Sticky posts first', 'xtweaks' ),
          'id'      => 'vb-sticky',
          'type'    => 'switch',
          'on'      => 'Yes',
          'off'     => 'No',
          'default' => false,
        ),

        //array(
        //'title'  => __('Pagination', 'xtweaks'),
        //'id'     => 'pagination-heading-start',
        //'type'   => 'section',
        //'indent' => false
        //),

        array(
          'title'  => __( 'Categories', 'xtweaks' ),
          'id'     => 'vb-categories-heading-start',
          'type'   => 'section',
          'class'  => ' heading',
          'indent' => true
        ),

        array(
          'title'   => __( 'Include categories', 'xtweaks' ),
          'id'      => 'vb-inc-cats',
          'type'    => 'select',
          // 'select2' => array( 'allowClear' => true ),
          'data'    => 'category',
          'multi'   => true
        ),

        array(
          'title'   => __( 'In ANY or ALL categories', 'xtweaks' ),
          'id'      => 'vb-all-cats',
          'type'    => 'button_set',
          'options' => array( 'any' => 'Any', 'all' => 'All' ),
          'default' => 'any',
        ),

        array(
          'title'   => __( 'Exclude categories', 'xtweaks' ),
          'id'      => 'vb-exc-cats',
          'type'    => 'select',
          'select2' => array( 'allowClear' => true ),
          'data'    => 'category',
          'multi'   => true
        ),

        array(
          'title'    => __( 'Include sub-categories on archives', 'xtweaks' ),
          'id'       => 'vb-sub-cats',
          'type'     => 'switch',
          'on'       => 'Yes',
          'off'      => 'No',
          'default'  => false,
          'subtitle' => 'This requires a specified post type, not Defaults'
        ),

        array(
          'id'     => 'vb-categories-section-end',
          'type'   => 'section',
          'indent' => false
        ),

        array(
          'title'  => __( 'Tags', 'xtweaks' ),
          'id'     => 'vb-tags-section-start',
          'type'   => 'section',
          'class'  => ' heading',
          'indent' => true
        ),

        array(
          'title' => __( 'Tags', 'xtweaks' ),
          'id'    => 'vb-inc-tags',
          'type'  => 'select',
          'data'  => 'tags',
          'multi' => true
        ),

        array(
          'title'   => __( 'Exclude tags', 'xtweaks' ),
          'id'      => 'vb-exc-tags',
          'type'    => 'select',
          'select2' => array( 'allowClear' => true ),
          'data'    => 'tags',
          'multi'   => true
        ),

        array(
          'id'     => 'tags-section-end',
          'type'   => 'section',
          'indent' => false
        ),

        array(
          'title'  => __( 'Custom taxonomies', 'xtweaks' ),
          'id'     => 'vb-custom-taxonomies-section-start',
          'type'   => 'section',
          'class'  => ' heading',
          'indent' => true
        ),

        // TODO: Add a loop to display all custom taxonomies
        // foreach($taxonomies as $taxonomy ){}
        array(
          'title'   => __( 'Other taxonomies', 'xtweaks' ),
          'id'      => 'vb-other-tax',
          'type'    => 'select',
          'select2' => array( 'allowClear' => true ),
          'data'    => 'taxonomies',
          'args'    => array( '_builtin' => false )
        ),

        array(
          'title'    => __( 'Other taxonomy terms', 'xtweaks' ),
          'id'       => 'vb-other-tax-tags',
          'type'     => 'select',
          'select2'  => array( 'allowClear' => true ),
          'data'     => 'callback',
          'multi'    => true,
          'args'     => array( 'xtw_get_tags_2' ),
          'subtitle' => __( 'Select terms to filter by in the chosen custom taxonomy', 'xtweaks' ),
          'desc'     => __( 'To populate this dropdown, select the Custom Taxonomy above, then Publish or Update this Blueprint', 'xtweaks' )
        ),
/**
* @todo
**/

        array(
          'title'    => __( 'Other taxonomy terms my way', 'xtweaks' ),
          'id'       => 'vb-other-tax-tags-my-way',
          'type'     => 'select',
          'select2'  => array( 'allowClear' => true ),
          'data'     => 'terms',
          'multi'    => true,
          'args'     => array('taxonomies' => array('property-categories')),
          // 'subtitle' => __( 'Select terms to filter by in the chosen custom taxonomy', 'xtweaks' ),
          // 'desc'     => __( 'To populate this dropdown, select the Custom Taxonomy above, then Publish or Update this Blueprint', 'xtweaks' )
        ),

        array(
          'title'   => __( 'Taxonomies operator', 'xtweaks' ),
          'id'      => 'vb-tax-operator',
          'type'    => 'button_set',
          'options' => array( 'AND' => 'All', 'IN' => 'Any', 'NOT IN' => 'None' ),
          'default' => 'IN',
          'hint'    => array( 'content' => __( 'Display posts containing all, any or none of the taxonomies', 'xtweaks' ) ),
        ),

        //TODO: Add taxomonies to exclude
        //    array(
        //      'title' => __('Days to show', 'xtweaks'),
        //      'id' => 'days',
        //      'type' => 'text',
        //      'cols'=>6,
        //              //      'default' => 'All',
        //    ),
        //    array(
        //      'title' => __('Days to show until', 'xtweaks'),
        //      'id' => 'days-until',
        //      'type' => 'text',
        //      'cols'=>6,
        //
        //    ),

        array(
          'id'     => 'vb-custom-taxonomies-section-end',
          'type'   => 'section',
          'indent' => false
        ),

        array(
          'title'  => __( 'Others', 'xtweaks' ),
          'id'     => 'vb-other-section-start',
          'type'   => 'section',
          'class'  => ' heading',
          'indent' => true
        ),

        array(
          'title'   => __( 'Authors', 'xtweaks' ),
          'id'      => 'vb-authors',
          'type'    => 'select',
          'data'    => 'callback',
          'args'    => array( 'xtw_get_authors' ),
          'default' => 'all',
        ),

        array(
          'id'     => 'other-section-end',
          'type'   => 'section',
          'indent' => false
        ),

      ),
    );

/**
Metaboxes Query
**/
    $metaboxes[ ] = array(
            'id'         => 'query-generator-metaboxes',
            'title'      => __('Query Generator', 'fusion-framework'),
            'post_types' => array('vb-query'),
            'position'   => 'normal', // normal, advanced, side
            'priority'   => 'high', // high, core, default, low
            'sidebar'    => false, // enable/disable the sidebar in the normal/advanced positions
            'sections'   => $page_options,
    );

    // Kind of overkill, but ahh well.  ;)
    //$metaboxes = apply_filters( 'your_custom_redux_metabox_filter_here', $metaboxes );

    return $metaboxes;
  }
  $redux_opt_name = "redux_tweaks";

  add_action("redux/metaboxes/{$redux_opt_name}/boxes", 'redux_add_metaboxes_45');
endif;


?>
